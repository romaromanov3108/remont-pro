<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname', 55);
            $table->string('phone', 55);
            $table->text('skills')->nullable();
            $table->integer('area_id')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('village_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('rating', 55)->nullable();
            $table->boolean('active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
