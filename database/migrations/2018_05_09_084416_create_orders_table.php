<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 111);
            $table->string('lastName', 111)->nullable();
            $table->string('phone', 12);
            $table->string('adres', 111)->nullable();
            $table->string('mail', 111)->nullable();
            $table->text('problem')->nullable();
            $table->string('forma_lend');
            $table->integer('suma')->nullable();
            $table->string('master_id', 111)->nullable();
            $table->string('status_id', 111)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
