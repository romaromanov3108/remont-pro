<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 111);
            $table->string('latin_url');
            $table->string('keywords');
            $table->string('prev_img');
            $table->text('description');
            $table->text('min_description');
            $table->text('article');
            $table->integer('id_category');
            $table->integer('view')->default(0);
            $table->integer('like')->default(0);
            $table->integer('comment')->default(0);
            $table->integer('autor_id');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
