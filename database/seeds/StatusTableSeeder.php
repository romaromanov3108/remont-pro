<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'не оброблений',
                'background' => 'orange',
                'group' => 0,
                'type' => 'order',
            ],
            [
                'title' => 'тільки консультація',
                'background' => 'brown',
                'group' => 1,
                'type' => 'order',
            ],
            [
                'title' => 'майстер визначений',
                'background' => 'grey',
                'group' => 1,
                'type' => 'order',
            ],
            [
                'title' => 'виконано',
                'background' => '#7BF549',
                'group' => 1,
                'type' => 'order',
            ],
            [
                'title' => 'відмова',
                'background' => 'red',
                'group' => 1,
                'type' => 'order',
            ],
            [
                'title' => 'закритий',
                'background' => 'green',
                'group' => 0,
                'type' => 'order',
            ],
            [
                'title' => 'не активований',
                'background' => 'orange',
                'group' => 0,
                'type' => 'user'
            ],
            [
                'title' => 'активний',
                'background' => 'green',
                'group' => 0,
                'type' => 'user'
            ],
            [
                'title' => 'заблокований',
                'background' => 'red',
                'group' => 0,
                'type' => 'user'
            ],
            [
                'title' => 'веріфікований',
                'background' => '#7BF549',
                'group' => 0,
                'type' => 'user'
            ],
        ];
        Status::insert($data);
    }
}
