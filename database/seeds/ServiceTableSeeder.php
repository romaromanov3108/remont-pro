<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Консультація по телефону',
                'description' => '',
            ],
            [
                'title' => ' Безкоштовний виїзд та діагностика',
                'description' => '',
            ],
            [
                'title' => 'Чистка від пилу',
                'description' => '',
            ],
            [
                'title' => 'Встановлення, перевстановлення операційної системи',
                'description' => '',
            ],
            [
                'title' => 'Заміна Мережевих конекторів',
                'description' => '',
            ],
            [
                'title' => 'Чистка від вірусів',
                'description' => '',
            ],
            [
                'title' => 'Відновлення стабільної роботи компютера',
                'description' => '',
            ],
            [
                'title' => 'Збір компютера під замовлення',
                'description' => '',
            ],
            [
                'title' => 'Настройка, телефонів, роутерів, та іншої цифрової техніки',
                'description' => '',
            ],

        ];

        Service::insert($data);
    }
}
