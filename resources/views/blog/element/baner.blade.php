<div class="banner">
    <div class="container">
        <h2>Сatharsis-service - Якісний сервіс за розумні гроші</h2>
        <p>Виконаємо все швидко, якісно, вміло та з гарантією на всі надані роботи!</p>
        <a href="/#fatures">Наші послуги</a>
    </div>
</div>