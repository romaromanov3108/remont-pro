@extends('blog.content')
@section('element')
    @foreach($articles as $article)
<div class="col-md-9 technology-left">
    <div class="business">
        <div class=" blog-grid2">
            <img src="/img/blog/articles/{{$article->latin_url}}/{{$article->prev_img}}" class="img-responsive" alt="{{$article->title}}">
            <div class="blog-text">
                <h5>{{$article->title}}</h5>
                <p>{!! $article->article !!}</p>
            </div>
        </div>
        <div class="blog-poast-info">
            <ul>
                <li><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#">{{$article->name}}</a></li>
                <li><i class="glyphicon glyphicon-calendar"> </i>{{$article->created_at}}</li>
                {{--<li><i class="glyphicon glyphicon-comment"> </i><a class="p-blog" href="#">{{$article->commnt}} </a></li>--}}
                <li><i class="like glyphicon glyphicon-heart" data-id="{{$article->id}}"
                       @if(\App\Like::where('id_article', $article->id)->where('id_user', Auth::user()->id)->first() !== null)
                        style="color: red;
                       @endif
                        "> </i><span class="count">{{$article->like}}</span></li>

                <li><i class="glyphicon glyphicon-eye-open"> </i>{{$article->view}}</li>
                <li style="float: right;">

                    <a href="{{ url('/auth/facebook') }}" style="float: right;" class="author"></a>
                    <a href="{{ url('/auth/google') }}" style="float: right;" class="author chr"></a>

                </li>
            </ul>
        </div>
        <div class="comment-top">
            <h2>Коментарі</h2>
        @foreach($comments as $comment)
            {{--<div class="media-left">--}}
                    {{--<img src="" >--}}
            {{--</div>--}}
            {{--<div class="media-body">--}}
                {{--<h4 class="media-heading"></h4>--}}
                {{--<p></p>--}}
                <!-- Nested media object -->
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="{{$comment->avatar}}" class="img-avatar" alt="{{$comment->name}}">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">{{$comment->name}} <span class="comment-date">{{$comment->date}}</span></h4>
                        <p>{{$comment->comment}}</p>
                        <!-- Nested media object -->
                    </div>
                </div>
                <!-- Nested media object -->
                {{--<div class="media">--}}
                    {{--<div class="media-left">--}}
                        {{--<a href="#">--}}
                            {{--<img src="images/si.png" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="media-body">--}}
                        {{--<h4 class="media-heading">Rackham</h4>--}}
                        {{--<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            @endforeach
        </div>
       @if(Auth::check())
        <div class="comment">
            <h3>Додати коментар</h3>

            <div class="media-left">

                    <img src="{{Auth::user()->avatar}}" class="img-avatar" alt="{{Auth::user()->name}}">

            </div>
            <div class="media-body">
                <h4 class="media-heading">{{Auth::user()->name}}</h4>
            </div>

            <div class=" comment-bottom">
                <form id="comment-form">
                    @csrf
                    <input type="hidden" name="article" value="{{$article->id}}">
                    {{--<input type="text" placeholder="Name">--}}
                    {{--<input type="text" placeholder="Email">--}}
                    {{--<input type="text" placeholder="Subject">--}}
                    <textarea placeholder="коментар" name="comment" class="form-control form-control-sm" style="color:#000;" required=""></textarea>
                    <input type="submit" id="btn-comment" value="Відправити">
                </form>
            </div>
        </div>
        @else
           <div style="width: 100%; height: 30px;"></div>
           <span>Для того щоб залишити коментарь потрібно авторизуватися</span>
            <a href="{{ url('/auth/facebook') }}" style="margin: 5px;" class="author"></a>
        @endif
    </div>
</div>
    @endforeach
@endsection