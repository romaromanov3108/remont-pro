@extends('blog.content')
@section('element')

    {{--<div style="width: 100%; height: 300px;">--}}
        {{--<div id="fb-root"></div>--}}
        {{--<script>(function(d, s, id) {--}}
                {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
                {{--if (d.getElementById(id)) return;--}}
                {{--js = d.createElement(s); js.id = id;--}}
                {{--js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";--}}
                {{--fjs.parentNode.insertBefore(js, fjs);--}}
            {{--}(document, 'script', 'facebook-jssdk'));</script>--}}

        {{--<!-- Your share button code -->--}}
        {{--<div class="fb-share-button"--}}
             {{--data-href="http://catharsis-service.com.ua/blog/atricle/ssd-vibor"--}}
             {{--data-layout="button_count">--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-9 technology-left">
        <div class="tech-no">
            <!-- technology-top -->
            @foreach($articles as $article)
                <div class="soci" data-url="{{$article->latin_url}}">
                    <ul>
                        <li><a href="http://www.twitter.com/share?url=" class="facebook-1"> </a></li>
                        <li><a href="https://plus.google.com/share?url=" class="facebook-1 twitter"> </a></li>
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=" class="facebook-1 chrome shareBtn"> </a></li>
                        <li><a href="http://www.twitter.com/share?url=" class="facebook-1 telegram"></a></li>
                    </ul>

                    {{--<script type="text/javascript">(function() {--}}
                            {{--if (window.pluso)if (typeof window.pluso.start == "function") return;--}}
                            {{--if (window.ifpluso==undefined) { window.ifpluso = 1;--}}
                                {{--var d = document, s = d.createElement('script'), g = 'getElementsByTagName';--}}
                                {{--s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;--}}
                                {{--s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';--}}
                                {{--var h=d[g]('body')[0];--}}
                                {{--// console.log(h);--}}
                                {{--// console.log(s);--}}
                                {{--console.log(h);--}}
                                {{--h.appendChild(s);--}}
                            {{--}})();</script>--}}
                    {{--<div class="pluso" data-background="transparent" data-options="big,square,line,vertical,nocounter,theme=04" data-services="facebook,twitter,google,vkontakte,odnoklassniki,email,print"></div>--}}
                </div>
                <div class="tc-ch">
                    <div class="tch-img">
                        <a href="/blog/atricle/{{$article->latin_url}}"><img src="/img/blog/articles/{{$article->latin_url}}/{{$article->prev_img}}" class="img-responsive" alt=""/></a>
                    </div>
                    {{--<a class="blog blue" href="singlepage.html">Technology</a>--}}
                    <h3><a href="/blog/atricle/{{$article->latin_url}}">{{$article->title}}</a></h3>
                    <p>{!! $article->min_description !!}</p>

                    <div class="blog-poast-info">
                        <ul>
                            <li><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#"> {{$article->name}} </a></li>
                            <li><i class="glyphicon glyphicon-calendar"> </i>{{$article->created_at}}</li>
                            {{--<li><i class="glyphicon glyphicon-comment"> </i><a class="p-blog" href="#">{{$article->comment}} </a></li>--}}
                            <li><i class="glyphicon glyphicon-heart"> </i><a class="admin" href="#">{{$article->like}} </a></li>
                            <li><i class="glyphicon glyphicon-eye-open"> </i>{{$article->view}}</li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- technology-top -->

                <!-- technology-top -->
            @endforeach
        </div>
    </div>

@endsection