<div class="col-md-3 technology-right">
    {{--<div class="blo-top">--}}
        {{--<div class="tech-btm">--}}
            {{--<img src="/img/blog/banner1.jpg" class="img-responsive" alt=""/>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="blo-top">
        <div class="tech-btm">
            <h4>Отримувати Новини</h4>
            <p>Підпишись на розсилку, отримай знижку на будь які наші послуги.</p>
            <div class="name">
                <form id="mailing_form">
                    @csrf
                    <input type="text" name="email" placeholder="Email" required="">
                </form>
            </div>
            <div class="button">
                <form>
                    <input type="submit" value="Підписатися" id="btn_mailing">
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="blo-top1">
        <div class="tech-btm">
            <h4>Кращі статті тижня!</h4>
            @foreach(\App\Articles::select('prev_img', 'title', 'latin_url', 'id')->orderBy('view', 'desc')->get() as $article)
            <div class="blog-grids">
                <div class="blog-grid-left">
                    <a href="/blog/atricle/{{$article->latin_url}}"><img src="/img/blog/articles/{{$article->latin_url}}/{{$article->prev_img}}" class="img-responsive" alt="{{$article->title}}"/></a>
                </div>
                <div class="blog-grid-right">

                    <h5><a href="/blog/atricle/{{$article->latin_url}}">{{$article->title}}</a> </h5>
                </div>
                <div class="clearfix"> </div>
            </div>
            @endforeach
            </div>
        </div>
    </div>

</div>