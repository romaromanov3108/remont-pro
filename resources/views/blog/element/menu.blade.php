<div class="head-bottom">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/blog">Головна</a></li>
                @foreach(\App\Categories::select('name', 'latin_url')->get() as $categories)
                <li><a href="{{$categories->latin_url}}">{{$categories->name}}</a></li>
                {{--<li><a href="videos.html">Відео</a></li>--}}
                {{--<li class="active"><a href="reviews.html">Новини</a></li>--}}

                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Статі<span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="tech.html">Action</a></li>--}}
                        {{--<li><a href="tech.html">Action</a></li>--}}
                        {{--<li><a href="tech.html">Action</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                @endforeach
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Culture <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="singlepage.html">Action</a></li>--}}
                        {{--<li><a href="singlepage.html">Action</a></li>--}}
                        {{--<li><a href="singlepage.html">Action</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Science <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="singlepage.html">Action</a></li>--}}
                        {{--<li><a href="singlepage.html">Action</a></li>--}}
                        {{--<li><a href="singlepage.html">Action</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li><a href="design.html">Design</a></li>--}}
                {{--<li><a href="business.html">Business</a></li>--}}
                {{--<li><a href="world.html">World</a></li>--}}
                {{--<li><a href="forum.html">Форум</a></li>--}}
                {{--<li><a href="contact.html">Контакти</a></li>--}}
            </ul>
        </div><!--/.nav-collapse -->
    </div>
    </nav>
</div>