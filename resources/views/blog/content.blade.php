@extends('blog')
@section('content')
    @include('blog.element.header')
    {{--@if(Request::path() == 'blog/ar')--}}
        @include('blog.element.baner')
    {{--@endif--}}
    <div class="technology">
        <div class="container">
            @yield('element')
            @include('blog.element.right-bar')
        <div class="clearfix"></div>
    <!-- technology-right -->

        </div>
    </div>
    @include('blog.element.footer')
    @include('element.alert')
@endsection



