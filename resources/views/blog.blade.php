<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="keywords" content="{{$keywords or 'ремонт компютера, ремонт кампютера, сервис, надому, чистка'}}">
        <meta name="description" content="{{$description or 'Catharsis-service Виконаємо все швидко, якісно, та вміло, надаємо гарантію на виконану роботу, безкоштовні поради щодо самостійного ремонту вдома, та цікаві статті'}}">
        <title>{{$title or env('APP_NAME')}}</title>

        <meta property="og:title" content="{{$title or 'Заголовок'}}" />
        <meta property="og:description" content="{{$minDesc or 'Опис'}}" />
        <meta property="og:url" content="{{Request::url()}}" />
        <meta property="og:image" content="{{$imgPath or 'http://catharsis-service.com.ua/img/catharsis.png'}}" />

        <meta name="title" content="{{$title or 'Заголовок'}}" />
        <meta name="description" content="{{$minDesc or 'Опис'}}" />
        <link rel="image_src" href="{{$imgPath or 'http://catharsis-service.com.ua/img/catharsis.png'}}" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href='/css/blog/fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
        <link href='/css/blog/fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

        <link href="/css/blog/style.css" rel="stylesheet">
        <script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        {{--<script src="/js/blog/facebook.js" async></script>--}}
    </head>
    <body>

        @yield('content')
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="/js/blog/main.js"></script>
    <script src="/js/blog/searsh.js"></script>
</html>