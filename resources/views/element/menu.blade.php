<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top"><img src="/img/logo1.png">{{env('APP_NAME')}}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                {{--<li>--}}
                    {{--<a class="page-scroll" href="#about">About</a>--}}
                {{--</li>--}}
                <li>
                    <a href="#" data-toggle="modal" data-target="#modal-form">Безкоштовна консультація</a>
                </li>
                <li>
                    <a class="page-scroll" href="#features">Послуги</a>
                </li>
                <li>
                    <a class="page-scroll" href="#about">Чому ми?</a>
                </li>
                {{--<li>--}}
                    {{--<a class="page-scroll" href="#faq">Часто винекаючі питання</a>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a class="page-scroll" href="#portfolio">Portfolio</a>--}}
                {{--</li>--}}
                <li>
                    <a class="page-scroll" href="#contact">Контакти</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>