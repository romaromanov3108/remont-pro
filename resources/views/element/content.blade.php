@extends('index')
@section('content')
    @include('element.menu')
    @include('element.header')
    @include('element.service')
    @include('element.about')
    {{--@include('element.faq')--}}
    @include('element.contact')
    {{--@include('element.foter')--}}
    @include('element.form_feedback')

    <a href="#" id="popup__toggle" onclick="return false;">
        <div class="circlephone" style="transform-origin: center;"></div>
        <div class="circle-fill" style="transform-origin: center;"></div>
        <div class="img-circle" style="transform-origin: center;">
            <div class="img-circleblock" style="transform-origin: center;"></div>
        </div>
    </a>

@endsection