<div class="feedback">
    <span>Що вас цікавить?</span>
    <button type="button" class="close" id="closed_feedback">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="form-feedback">
        <form id="form-feedback">
            @csrf
            <div class="form-group">
                <input type="text" name="name" class="form-control form-control-sm" placeholder="Ім'я*">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control form-control-sm" placeholder="email*">
            </div>
            <div class="form-group">
                <input type="text" name="phone" class="form-control form-control-sm" placeholder="Номер телефону">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="question" style="height: 100px;" placeholder="Ваше запитання*"></textarea>
            </div>
            <button class="btn btn-primary btn-sm" id="btn_feedback">Відправити</button>
        </form>

        <span class="feedback-text"><i class="fa fa-check-circle" style="color: green; font-size: 13px"></i> Напишіть нам і ми обовязково вам відповімо.</span>
    </div>
</div>