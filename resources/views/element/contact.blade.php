<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">Контактна інформація!</h2>
                <hr class="primary">
                <p>Якщо в вас залишилися будь які запитання ви можете написати нам, або зателефонувати</p>
            </div>
            <div class="col-lg-4 col-lg-offset-2 text-center">
                <i class="fa fa-phone fa-3x sr-contact"></i>
                <p>+380-67-260-32-84 <br>
                    +380-99-228-64-71
                </p>

            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                <p><a href="mailto:catharsis.service@gmail.com">catharsis.service@gmail.com</a></p>
            </div>
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <a href="https://www.facebook.com/catharsis.service/" target="_blank"><i class="my_icon logo-facebook"></i><a>
                <a href="https://www.instagram.com/catharsis.service/?utm_source=ig_profile_share&igshid=1dqfwu8h33k0b" target="_blank"><i class="my_icon logo-instagram"></i><a>
            </div>
        </div>
    </div>
</section>