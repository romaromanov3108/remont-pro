<section id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 preamble">
                <h3>Наші послуги</h3>
                <p class="lead">На всі проведені нами роботи надається гарантія!</p>
            </div>
            @foreach($services as $service)
            <div class="col-md-6 feature">
                <div class="feature-icon">
                    <img src="/img/service/{{$service->img}}">
                </div>
                <div class="feature-content">
                    <h4><b style="color: #f05f40;">{{$service->title}}</b></h4>
                    <p>{{$service->description}}</p>
                </div>
            </div>
            @endforeach

            <div class="container" style="clear: both;">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    {{--<h4><b>Безкоштовна консультація</b></h4>--}}
                <form method="post" id="form-service">
                    @csrf
                    <input type="hidden" name="forma" value="form-service">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control form-control-sm" placeholder="Ім'я">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control form-control-sm" placeholder="номер телефону">
                    </div>
                    <button class="btn btn-primary btn-sm" id="btn_service">Безкоштовна консультація</button>
                </form>
                </div>
            </div>


        </div>
    </div>
</section>