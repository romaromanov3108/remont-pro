<section id="faq">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 preamble">
                <h3>Часто винекаючі питання</h3>
                <p class="lead">Питання які найчастіше задають нам наші користувачі</p>
            </div>
            <div class="col-md-6">
                <div class="question">
                    <i class="fa fa-comment"></i><h4 class="title">Діагностика та виклик майстра безкоштовна?</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione doloremque quidem deserunt aperiam qui molestias officiis nisi ut aspernatur aliquam eveniet, porro eius iure eligendi laboriosam dolores, nobis sunt consectetur?</p>
                </div>
                <div class="question">
                    <i class="fa fa-comment"></i><h4 class="title">Чи можна у вас провести апгрейд свого компютера?</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione doloremque quidem deserunt aperiam qui molestias officiis nisi ut aspernatur aliquam eveniet, porro eius iure eligendi laboriosam dolores, nobis sunt consectetur?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="question">
                    <i class="fa fa-comment"></i><h4 class="title">When wordpress version will be available?</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione doloremque quidem deserunt aperiam qui molestias officiis nisi ut aspernatur aliquam eveniet, porro eius iure eligendi laboriosam dolores, nobis sunt consectetur?</p>
                </div>
                <div class="question">
                    <i class="fa fa-comment"></i><h4 class="title">Are you able to hire?</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione doloremque quidem deserunt aperiam qui molestias officiis nisi ut aspernatur aliquam eveniet, porro eius iure eligendi laboriosam dolores, nobis sunt consectetur?</p>
                </div>

            </div>
        </div>
    </div>
</section>