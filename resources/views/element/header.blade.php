

<header>
    <div class="header-content">
        <div class="row">
            <div class="hero-text">
                <div class="col-md-6">
                    <h3 class="title">{{env('APP_NAME')}}</h3>
                    <p class="lead">
                        Виконаємо все швидко, якісно, та вміло, надаємо гарантію на виконану роботу.
                    </p>
                    <ul class="custom-list">
                        <li><i class="fa fa-check"></i><span> Безкоштовна консультація по телефону</span></li>
                        <li><i class="fa fa-check"></i><span> Безкоштовний виїзд та діагностика</span></li>
                        <li><i class="fa fa-check"></i><span> Чистка від пилу</span></li>
                        <li><i class="fa fa-check"></i><span> Встановлення, перевстановлення операційної системи</span></li>
                        <li><i class="fa fa-check"></i><span> Заміна Мережевих конекторів</span></li>
                        <li><i class="fa fa-check"></i><span> Чистка від вірусів</span></li>
                        <li><i class="fa fa-check"></i><span> Відновлення стабільної роботи компютера</span></li>
                        <li><i class="fa fa-check"></i><span> Збір компютера під замовлення</span></li>
                        <li><i class="fa fa-check"></i><span> Настройка телефонів, роутерів, та іншої цифрової техніки</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 col-md-offset-1">
                <div class="form-header">
                    <h4>Безкоштовна консультація з майстром!</h4>
                    <form id="header_form">
                        @csrf
                        <input type="hidden" name="forma" value="header_form">
                        <div class="form-group-sm">
                            <input type="text" name="name" class="form-control form-control-sm" placeholder="Ваше імя" required>
                        </div>
                        <div style="width: 100%; height: 10px;"></div>
                        <div class="form-group-sm">
                            <input type="text" name="phone" class="form-control form-control-sm" placeholder="Номер телефону" required>
                        </div>
                        <div style="width: 100%; height: 10px;"></div>
                        <button class="btn btn-primary btn-sm" id="btn_header_form" style="padding: 4px 40px;">Замовити</button>
                    </form>
                    <h6><i class="fa fa-check-circle" style="color: green; font-size: 13px"></i> Ми вам обовязково зателефонуємо</h6>
                </div>

            </div>
        </div>
    </div>
</header>
@include('element.modal.form')
