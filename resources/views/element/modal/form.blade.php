<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--<h3 class="modal-title" id="exampleModalLabel"><b>Безкоштовна консультація</b></h3>--}}
                <span><b>Безкоштовна консультація</b></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="upload-form">
                    <form id="form-call" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="forma" value="form-call">
                        <div class="form-group">

                            <input type="text" class="form-control form-control-sm" name="name" aria-describedby="titleHelp" id="title-sup" required placeholder="Ім'я">

                        </div>
                        <div class="form-group">

                            <input type="text" class="form-control form-control-sm" name="phone" aria-describedby="emailHelp" id="email-sup" required placeholder="Номер телефону">

                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn_alert">Замовити</button>
            </div>
        </div>
    </div>
</div>
