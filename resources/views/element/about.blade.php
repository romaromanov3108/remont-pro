<section id="about" class="our-courses mt-30 mb-50">
    <h2 class="title-about">Чому обирають саме нас?</h2>
    <div style="width: 100%; height: 50px;"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <figure class="pull-left icon-check"><i style="font-size:80px; color:#5A9BD1" class="fa fa-check-circle" aria-hidden="true"></i></figure>
                <h3><b>Гарнатія</b></h3>
                <p class="about-desc">На КОЖНУ надану нами послуг ми надаємо гарантію
                    та в випадку настання гарантійного випадку гарантуємо безкоштовне виправлення проблеми.</p>
            </div>
            <div class="col-sm-4">
                <figure class="pull-left icon-check"><i style="font-size:80px; color:#62d037" class="fa fa-check-circle" aria-hidden="true"></i></figure>
                <h3><b>Безкоштовна консультація</b></h3>
                <p class="about-desc">Ми розглядаємо кожне звернення та стараємось допомогти в телефонному режимі абсолютно безкоштовно.</p>
            </div>
            <div class="col-sm-4">
                <figure class="pull-left icon-check"><i style="font-size:80px; color:#f05f40" class="fa fa-check-circle" aria-hidden="true"></i></figure>
                <h3><b>Комфорт</b></h3>
                <p class="about-desc">Вам не потрібно нікуди нести свій девайс достатньо всього лиш залишити заявку і наш майстер зв'яжеться з вами
                    в випадку якщо проблему не вдасться вирішити в телефонному режимі ви зможете узгодити час на виїзд майстра (виїзд майстра та діагностика безкоштовна).
                    <br><b>Майже усі роботи проводяться на дому в присутності клієнта. Кінцева вартість встановлюється ДО початку робіт.</b>
                </p>
            </div>
        </div>
    </div>
    <div style="width: 100%; height: 50px;"></div>
    <div class="container" style="clear: both;">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            {{--<h4><b>Безкоштовна консультація</b></h4>--}}
            <form method="post" id="form-about">
                @csrf
                <input type="hidden" name="forma" value="form-about">
                <div class="form-group">
                    <input type="text" name="name" class="form-control form-control-sm" placeholder="Ім'я">
                </div>
                <div class="form-group">
                    <input type="text" name="phone" class="form-control form-control-sm" placeholder="номер телефону">
                </div>
                <button class="btn btn-primary btn-sm" id="btn_about">Безкоштовна консультація</button>
            </form>
        </div>
    </div>
</section>