<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content="Виконаємо все швидко, якісно, та вміло, надаємо гарантію на виконану роботу."/>
        <meta name="author" content="{{env('APP_NAME')}}">
        <meta name="google-site-verification" content="JNVUF9HNy_zuPWQTAfXaUSvU_KcadHZQCZi0P2fkZQg" />

        <!-- Facebook -->

        <meta property="og:image" content="http://catharsis-service.com.ua/img/catharsis.png"/>
        <meta property="og:url" content="http://catharsis-service.com.ua/"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="Catharsis-service"/>
        <meta property="og:description" content="Виконаємо все швидко, якісно, та вміло, надаємо гарантію на виконану роботу."/>
        <meta property="fb:app_id" content="443902636100298">

        <title>{{env('APP_NAME')}}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <!-- Custom Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <!-- Plugin CSS -->

        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" rel="stylesheet">
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
        <!-- Theme CSS -->
        <link href="/css/creative.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126290002-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-126290002-1');
        </script>

    </head>
    <body id="page-top">
        @yield('content')
        @include('element.alert')
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="/js/creative.min.js"></script>
    <script src="/js/main.js"></script>
</html>