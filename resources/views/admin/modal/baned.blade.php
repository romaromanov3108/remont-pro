<div class="modal fade" id="modal-baned-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--<h3 class="modal-title" id="exampleModalLabel"><b>Безкоштовна консультація</b></h3>--}}
                <span><b>Забанити</b></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="col-8" style="margin: 0 auto;">
                <form id="baned-form">
                    @csrf
                    <input type="hidden" name="id" value="{{$master->id}}">
                    <div class="form-group">
                        <label for="ban-reson">Причина бану</label>
                        <textarea name="reson" id="ban-reson" class="form-control form-control-sm" placeholder="Не виконання гарантійного випадку" style="height: 150px"></textarea>
                    </div>
                </form>
               </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-sm" id="btn-baned-form">Забанити</button>
            </div>
        </div>
    </div>
</div>