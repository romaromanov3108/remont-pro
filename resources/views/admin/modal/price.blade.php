<div class="modal fade" id="modal-price" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--<h3 class="modal-title" id="exampleModalLabel"><b>Безкоштовна консультація</b></h3>--}}
                <span><b>Прайс</b></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Послуга</th>
                            <th>ціна</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Діагностика на дому</td>
                            <td>0 грн</td>
                        </tr>
                        <tr>
                            <td>Чистка від пилюки з заміною термопасти</td>
                            <td>230 грн</td>
                        </tr>
                        <tr>
                            <td>Чистка від вірусів</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Настройка роутера</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Відновлення роботи (приберання зависань)</td>
                            <td>150грн</td>
                        </tr>
                        <tr>
                            <td>Заміна мережевих конекторів</td>
                            <td>50грн</td>
                        </tr>
                        <tr>
                            <td>Встановлення Операційної системи</td>
                            <td>200 грн</td>
                        </tr>
                        <tr>
                            <td>Налаштування телефонів, скидання на заводські установк</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Встановлення програмного забезпечення</td>
                            <td></td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>