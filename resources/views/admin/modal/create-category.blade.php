<div class="modal fade" id="modal-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--<h3 class="modal-title" id="exampleModalLabel"><b>Безкоштовна консультація</b></h3>--}}
                <span><b>Стоврити категорію</b></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create_categories">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="name" class="form-control form-control-sm" placeholder="Назва категорії" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="latin_url" class="form-control form-control-sm" placeholder="/home" required>
                    </div>
                </form>
                <button class="btn btn-success btn-sm" id="btn_create_categories">Додати категорію</button>
            </div>
        </div>
    </div>
</div>