<div class="modal fade" id="modal-read" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{--<h3 class="modal-title" id="exampleModalLabel"><b>Безкоштовна консультація</b></h3>--}}
                <span><b>Підтвердити виконання заказу</b></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="upload-form">
                    <form id="form-confirm" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="order" value="{{$order->id}}">
                        <div class="form-group">
                            <textarea class="form-control form-control-sm" style="height: 100px;" name="comment" placeholder="Коментар" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="file_form">Зображення підписаної накладної*</label>
                            <input type="file" name="photo" id="file_form" required>
                        </div>
                        {{--<div class="form-check">--}}
                            {{--<label class="form-check-label" for="money_cheked">Гроші перечислив</label>--}}
                            {{--<input type="checkbox" class="form-check" id="money_cheked">--}}
                        {{--</div>--}}
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-sm" id="btn-form-comfirm">Підтвердити</button>
            </div>
        </div>
    </div>
</div>
