@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <table class="table table-sm">
        <thead>
            <th scope="col">№</th>
            <th scope="col">Ім'я</th>
            <th scope="col">Номер</th>
            <th scope="col">сума(грн)</th>
            <th scope="col">коментарі</th>
            <th scope="col">майстер</th>
            <th scope="col">підтвердження(коментар)</th>
            <th scope="col">статус(при закриванні)</th>

            <th>дії</th>
        </thead>
        <tbody>
           @foreach($orders as $order)
            <tr
                    @if($order->closed == 1)
                    class="bg-success"
                    style="color: #fff;"
                    @endif
            >
                <td>{{$order->id}}</td>
                <td>{{$order->name}}</td>
                <td>{{$order->phone}}</td>
                <td>{{$order->suma}} грн</td>
                <td>{{$order->problem}}</td>
                <td>{{\App\User::where('id', $order->master_id)->value('name')}}</td>
                <td>{{$order->comment}}</td>
                <td style="text-align: center; background:{{\App\Status::where('id', $order->status_id)->value('background')}};">
                    <span style="color: #fff;">{{\App\Status::where('id', $order->status_id)->value('title')}}</span>
                </td>
                <td>
                    <a href="/admin/confirm/order/{{$order->id}}" class="btn btn-primary btn-sm"><ion-icon name="more"></ion-icon></a>
                </td>
            </tr>
           @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning">Ви не є адміністратором тому даний розділ для вас закритий!</div>
    @endif
@endsection