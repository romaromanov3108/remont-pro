@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <a class="btn btn-primary btn-sm" href="/admin/success-verification" style="float: right; margin: 10px 0px;">Запити на верифікацію
        <span class="badge badge-warning">{{count(\App\Master_verification::where('verification', 0)->get())}}</span></a>
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Ім'я</th>
                <th scope="col">Фамілія</th>
                <th scope="col">Номер</th>
                <th scope="col">Емайл</th>
                <th scope="col">замовлень</th>
                <th scope="col">закритих замовлень</th>
                <th scope="col">Бани</th>
                {{--<th scope="col">Рейтинг</th>--}}
                <th scope="col">Статус</th>
                <th scope="col">Дії</th>
            </tr>
        </thead>
        <tbody>
          @foreach($masters as $master)
            <tr>
                <td>{{$master->id}}</td>
                <td>{{$master->name}}</td>
                <td>{{$master->surname}}</td>
                <td>{{$master->phone}}</td>
                <td>{{$master->email}}</td>
                <td>{{count(\App\Order::where('master_id', $master->id)->get())}}</td>
                <td>{{count(\App\Order::where('master_id', $master->id)->where('closed', 1)->get())}}</td>
                <td>{{count(\App\Baned::where('user_id', $master->id)->get())}}</td>
                {{--<td>{{$master->rating}}</td>--}}
                <td style="text-align: center; background:{{\App\Status::where('id', $master->status_id)->value('background')}};">
                    <span style="color: #fff;">{{\App\Status::where('id', $master->status_id)->value('title')}}</span>
                </td>
                <td>
                   @if($master->status_id != 8)
                    <a href="/admin/master/active/{{$master->id}}" class="btn btn-success btn-sm btn-click"><ion-icon name="checkmark"></ion-icon></a>
                   @endif
                    <a href="/admin/master/detail/{{$master->id}}" class="btn btn-primary btn-sm"><ion-icon name="more"></ion-icon></a>
                    @if($master->status_id != 7 or $master->status_id = 9)
                    <a href="/admin/master/deactive/{{$master->id}}" class="btn btn-warning btn-sm btn-click"><ion-icon name="sad"></ion-icon></a>
                    @endif
                    {{--<a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-baned-form"><ion-icon name="remove-circle-outline"></ion-icon></a>--}}

                </td>
            </tr>
          @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning">Ви не є адміністратором тому даний розділ для вас закритий!</div>
    @endif
    {{--@include('admin.modal.baned')--}}
@endsection