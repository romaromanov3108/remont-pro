@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
        <a href="/admin/blog"></a>

        <table class="table table-sm">
            <thead>
            <tr>
                <th>ID</th>
                <th>Категорія</th>
                <th>url</th>
                <th style="width: 20%">дії</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>{{$category->latin_url}}</td>
                    <td>
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-pod-cat"><ion-icon name="md-add"></ion-icon></button>
                        <button class="btn btn-info btn-sm"><ion-icon name="md-create"></ion-icon></button>
                        <button class="btn btn-danger btn-sm"><ion-icon name="md-trash"></ion-icon></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="/admin/blog/categories/create" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-category" style="width: 100%;">Додати категорію</a>

@include('admin.modal.create-category')
@include('admin.modal.create-pod-cat')
    @endif
@endsection