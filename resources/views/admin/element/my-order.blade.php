@extends('admin.content')
@section('element')
    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Ім'я</th>
            <th scope="col">Номер</th>
            <th scope="col">Адреса</th>
            {{--<th scope="col">Емайл</th>--}}
            <th scope="col">Коментарі</th>
            <th scope="col">Майстер</th>
            <th scope="col">Сума</th>
            <th scope="col">Статус</th>
            <th scope="col">Дата</th>
            <th scope="col">Дії</th>

        </tr>
        </thead>
        <tbody>
        @foreach($data as $order)
            <tr>
                <th scope="row">{{$order->id}}</th>
                <td>{{$order->name}}</td>
                <td>{{$order->phone}}</td>
                <td>{{$order->adres}}</td>
                {{--<td>romaromanov3108@gmail.com</td>--}}
                <td>{{$order->problem}}</td>
                <td>
                    {{\App\User::where('id', $order->master_id)->value('name')}}
                </td>
                <td>{{$order->suma.'грн'}}</td>
                <td style="text-align: center; background:{{\App\Status::where('id', $order->status_id)->value('background')}};">
                    <span style="color: #fff;">{{\App\Status::where('id', $order->status_id)->value('title')}}</span>
                </td>
                <td>{{$order->created_at}}</td>
                <td>
                    @if(\App\User::where('id', Auth::id())->value('group') < 1)
                        <a href="/admin/read/my/{{$order->id}}" class="btn btn-primary btn-sm"><ion-icon name="create"></ion-icon></a>
                    @else
                        @if($order->status_id == 1 or $order->master_id == Auth::id())
                            <a href="/admin/read/my/{{$order->id}}" class="btn btn-success btn-sm"><ion-icon name="more"></ion-icon></a>
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @endsection