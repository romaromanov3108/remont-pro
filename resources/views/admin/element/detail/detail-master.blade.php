@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <div class="container">
        <div class="row justify-content-md-center">

            <div class="col-md-6">
                   @foreach($masters as $master)
                    <table class="table char-mods table-sm">
                        <tbody>
                            <tr>
                                <td>Імя:</td>
                                <td>{{$master->name}}</td>
                            </tr>
                            <tr>
                                <td>Фамілія:</td>
                                <td>{{$master->surname}}</td>
                            </tr>
                            <tr>
                                <td>Номер телефону:</td>
                                <td>{{$master->phone}}</td>
                            </tr>
                            <tr>
                                <td>Емайл:</td>
                                <td>{{$master->email}}</td>
                            </tr>
                            <tr>
                                <td>Про себе (навики):</td>
                                <td>{{$master->skills}}</td>
                            </tr>
                            <tr>
                                <td>Кількість банів</td>
                                <td>{{count(\App\Baned::where('user_id', $master->id)->get())}}</td>
                            </tr>
                            <tr>
                                <td>Замовлень (загалом):</td>
                                <td>{{count(\App\Order::where('master_id', $master->id)->get())}}</td>
                            </tr>
                            <tr>
                                <td>Закритих замовлень):</td>
                                <td>{{count(\App\Order::where('master_id', $master->id)->where('closed', 1)->get())}}</td>
                            </tr>
                            <tr>
                                <td>Статус:</td>
                                <td style="text-align: center; background:{{\App\Status::where('id', $master->status_id)->value('background')}};">
                                    <span style="color: #fff;">{{\App\Status::where('id', $master->status_id)->value('title')}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>Зареєструвався:</td>
                                <td>{{$master->created_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                    @if($master->status_id != 8)
                        <a href="/admin/master/active/{{$master->id}}" class="btn btn-success btn-sm btn-click"><ion-icon name="checkmark"></ion-icon> Активувати</a>
                    @endif
                    @if($master->status_id != 7 or $master->status_id = 9)
                        <a href="/admin/master/deactive/{{$master->id}}" class="btn btn-warning btn-sm btn-click"><ion-icon name="sad"></ion-icon> Деактивувати</a>
                    @endif
                    <a href="/admin/baned/" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-baned-form"><ion-icon name="remove-circle-outline"></ion-icon> Заблокувати</a>
                    <button class="btn btn-info btn-sm" type="button" data-toggle="collapse" data-target="#collapse-baned"><ion-icon name="sad"></ion-icon> Бани</button>
                    <div style="width: 100%; height: 40px;"></div>
                    @include('admin.collapse.baned')
            </div>

        </div>
    </div>

    <div style="width: 100%; height: 40px;"></div>

        @if(count($orders) > 0)
            <h3> Замовлення майстра </h3>
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Ім'я</th>
                            <th scope="col">Номер</th>
                            <th scope="col">Адреса</th>
                            {{--<th scope="col">Емайл</th>--}}
                            <th scope="col">Коментарі</th>
                            <th scope="col">Майстер</th>
                            <th scope="col">Сума</th>
                            <th scope="col">Статус</th>
                            <th scope="col">Дата</th>
                            <th scope="col">Дії</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <th scope="row">{{$order->id}}</th>
                                <td>{{$order->name}}</td>
                                <td>{{$order->phone}}</td>
                                <td>{{$order->adres}}</td>
                                {{--<td>romaromanov3108@gmail.com</td>--}}
                                <td>{{$order->problem}}</td>
                                <td>
                                    {{\App\User::where('id', $order->master_id)->value('name')}}
                                </td>
                                <td>{{$order->suma.'грн'}}</td>
                                <td style="text-align: center; background:{{\App\Status::where('id', $order->status_id)->value('background')}};">
                                    <span style="color: #fff;">{{\App\Status::where('id', $order->status_id)->value('title')}}</span>
                                </td>
                                <td>{{$order->created_at}}</td>
                                <td>
                                    <a href="/admin/confirm/order/{{$order->id}}" class="btn btn-primary btn-sm"><ion-icon name="more"></ion-icon></a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
        @else
            <h3 style="text-align: center;">В даного майстра ще немає замовлень!</h3>
        @endif
    @include('admin.modal.baned')
                    @endforeach
    @else
        <div class="alert alert-warning">Ви не є адміністратором тому даний розділ для вас закритий!</div>
    @endif

@endsection