@extends('admin.content')
@section('element')
        <table class="table table-sm">
                <thead>
                <tr>
                        <th scope="col">#</th>
                        <th scope="col">Ім'я</th>
                        <th scope="col">Фамілія</th>
                        <th scope="col">Номер</th>
                        <th scope="col">Емайл</th>
                        <th scope="col">Фото документів</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Дії</th>
                </tr>
                </thead>
                <tbody>
                        @foreach($master as $verifi)
                        <tr>
                                <td>{{$verifi->master_id}}</td>
                                <td>{{$verifi->name}}</td>
                                <td>{{$verifi->surname}}</td>
                                <td>{{$verifi->phone}}</td>
                                <td>{{$verifi->email}}</td>
                                <td>
                                        @foreach(explode(',', $verifi->document_photo) as $img)
                                                <img src="/img/verification/user/{{$verifi->master_id}}/{{$img}}" style="width: 50px; height: 50px;">
                                        @endforeach
                                </td>
                                <td style="text-align: center; background:{{\App\Status::where('id', $verifi->status_id)->value('background')}};">
                                        <span style="color: #fff;">{{\App\Status::where('id', $verifi->status_id)->value('title')}}</span>
                                </td>
                                <td><a href="/admin/verifi-master/{{$verifi->master_id}}" class="btn btn-success btn-sm">Підтвердити</a></td>
                        </tr>
                        @endforeach
                </tbody>
        </table>
@endsection