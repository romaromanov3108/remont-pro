@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
        <div class="col-md-8">
            <h4>Редагувати статтю {{$articles[0]->title}}</h4>
    @foreach($articles as $article)
        <form id="article-read-form" method="post" multiple="true">
            <input type="hidden" name="id" value="{{$article->id}}">
            @csrf
            <div class="form-group">
                {{--<label>Заголовок</label>--}}
                <input type="text" name="title" class="form-control form-control-sm" value="{{$article->title}}" placeholder="Заголовок" required>
            </div>
            <div class="form-group">
                {{--<label>Ключові слова</label>--}}
                <input type="text" name="keywords" class="form-control form-control-sm" value="{{$article->keywords}}" placeholder="Ключові слова" required>
            </div>
            <div class="form-group">
                {{--<label>Ключові слова</label>--}}
                <input type="text" name="latin_url" class="form-control form-control-sm" value="{{$article->latin_url}}" placeholder="/latin name" required>
            </div>
            <div class="form-group">
                {{--<label>Короткий опис</label>--}}
                <textarea name="description" class="form-control form-control-sm" placeholder="Опис (для пошуку)" style="min-height: 100px" required>{{$article->description}}</textarea>
            </div>
            <div class="form-group">
                {{--<label>Короткий опис</label>--}}
                <textarea name="min_description" class="form-control form-control-sm" placeholder="короткий опис" style="min-height: 100px" required>{{$article->min_description}}</textarea>
            </div>
            <div class="form-group">
                {{--<label>Короткий опис</label>--}}
                <textarea id="summernote" name="article" class="form-control form-control-sm" placeholder="Статя">{{$article->article}}</textarea>
            </div>
            <div class="form-group">
                <select name="id_category" class="form-control form-control-smm">
                    <option value="{{$article->id_category}}">{{\App\Categories::where('id', $article->id_category)->value('name')}}</option>
                    @foreach(\App\Categories::all() as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Фото превю</label>
                <input type="file" name="prev_img">
            </div>
        </form>
        <button class="btn btn-success btn-sm" id="btn_article_read">зберегти</button>
        </div>
        <div class="col-md-12" style="height: 40px;"></div>
        <script>
            $(document).ready(function(){
                $('#summernote').summernote({
                    height: 500,
                    callbacks: {
                        onImageUpload: function (files) {
                            var el = $(this);
                            sendFile(files[0], el);
                        }
                    }

                });
                function sendFile(file, el) {
                    var  data = new FormData();
                    data.append("file", file);
                    var url = '/admin/blog/sumernote/upload';
                    $.ajax({
                        data: data,
                        type: "POST",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: url,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(url2) {
                            el.summernote('insertImage', url2);
                        }
                    });
                }
            });
        </script>


    @endforeach
    </div>
    @endif
@endsection