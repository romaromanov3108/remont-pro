@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <div class="col-lg-8 col-lg-offset-2">
     <h3 style="text-align: center;">Створити статтю</h3>
        {{--<form method="post" action="/admin/blog/create/test">--}}
            {{--@csrf--}}
            {{--<div class="form-group">--}}
                {{--<input type="text" name="name" class="form-control">--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<input type="text" name="email" class="form-control">--}}
            {{--</div>--}}
            {{--<input type="submit" class="btn btn-success" value="send">--}}
        {{--</form>--}}
     <form id="article-form" method="post" action="/admin/blog/create/articles" multiple="true">
        @csrf
        <div class="form-group">
            {{--<label>Заголовок</label>--}}
            <input type="text" name="title" class="form-control form-control-sm" placeholder="Заголовок" required>
        </div>
         <div class="form-group">
             {{--<label>Ключові слова</label>--}}
             <input type="text" name="keywords" class="form-control form-control-sm" placeholder="Ключові слова" required>
         </div>
         <div class="form-group">
             {{--<label>Ключові слова</label>--}}
             <input type="text" name="latin_url" class="form-control form-control-sm" placeholder="/latin name" required>
         </div>
         <div class="form-group">
             {{--<label>Короткий опис</label>--}}
             <textarea name="description" class="form-control form-control-sm" placeholder="Опис (для пошуку)" style="min-height: 100px" required></textarea>
         </div>
         <div class="form-group">
             {{--<label>Короткий опис</label>--}}
             <textarea name="min_description" class="form-control form-control-sm" placeholder="короткий опис" style="min-height: 100px" required></textarea>
         </div>
         <div class="form-group">
             {{--<label>Короткий опис</label>--}}
             <textarea id="summernote" name="article" class="form-control form-control-sm" placeholder="Статя"></textarea>
         </div>
         <div class="form-group">
             <select name="id_category" class="form-control form-control-smm">
                @foreach(\App\Categories::all() as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
             </select>
         </div>
         <div class="form-group">
             <label>Фото превю</label>
             <input type="file" name="prev_img">
         </div>
     </form>
        <button class="btn btn-success btn-sm" id="btn_article_cr">Создать</button>
    </div>
    <div class="col-md-12" style="height: 40px;"></div>
    <script>
        $(document).ready(function(){
        $('#summernote').summernote({
            height: 500,
            callbacks: {
                onImageUpload: function (files) {
                    var el = $(this);
                    sendFile(files[0], el);
                }
            }

        });
        function sendFile(file, el) {
            var  data = new FormData();
            data.append("file", file);
            var url = '/admin/blog/sumernote/upload';
            $.ajax({
                data: data,
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                success: function(url2) {
                    el.summernote('insertImage', url2);
                }
            });
        }
        });
    </script>
    @endif
@endsection