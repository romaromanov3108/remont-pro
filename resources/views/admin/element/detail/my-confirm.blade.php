@extends('admin.content')
@section('element')
    <div class="container">
        <div class="row justify-content-md-center">

            <div class="col-md-6">
                @foreach($orders as $order)
                    <h4 style="text-align: center;">підтвердження Заказ №{{$order->id}}</h4>
                    <table class="table char-mods table-sm">
                        <tbody>
                           <tr>
                              <td style="border-top: 0px;">Ім'я:</td>
                              <td style="border-top: 0px;">{{$order->name}}</td>
                           </tr>
                           <tr>
                              <td>Телефон:</td>
                              <td>{{$order->phone}}</td>
                           </tr>
                           <tr>
                               <td>Адреса:</td>
                               <td>{{$order->adres}}</td>
                           </tr>
                           <tr>
                               <td>Коментарі:</td>
                               <td>{{$order->problem}}</td>
                           </tr>

                           <tr>
                               <td>Майстер:</td>
                               <td>{{\App\User::where('id', $order->master_id)->value('name')}}</td>
                           </tr>
                           <tr>
                               <td>Сума:</td>
                               <td>{{$order->suma}} грн</td>
                           </tr>
                           <tr>
                               <td>Заказ зроблено:</td>
                               <td>{{$order->created_at}}</td>
                           </tr>
                           <tr>
                               <td>Cтатус з яким закритий:</td>
                               <td style="text-align: center; background:{{\App\Status::where('id', $order->status_id_confirm)->value('background')}};">
                                   <span style="color: #fff;">{{\App\Status::where('id', $order->status_id_confirm)->value('title')}}</span>
                               </td>
                           </tr>
                           <tr>
                               <td>Cтатус:</td>
                               @if($order->status_id == 6)
                               <td style="text-align: center; background:{{\App\Status::where('id', $order->status_id)->value('background')}};">
                                   <span style="color: #fff;">{{\App\Status::where('id', $order->status_id)->value('title')}}</span>
                               </td>
                               @else
                                <td style="text-align: center; background: #880000;">
                                  <span style="color: #fff;">Не закритий</span>
                                </td>
                               @endif
                           </tr>
                           <tr>
                               <td>Коментар при підтвердженні:</td>
                               <td>{{$order->comment}}</td>
                           </tr>
                        </tbody>
                    </table>
                    <div class="block-img">
                        <span>Фото накладної</span>
                        <img src="/img/confirm-order/user/{{$order->photo}}" style="max-width: 500px;">
                    </div>
                    <button class="btn btn-success" id="btn-confirm-admin" style="margin-bottom: 50px;"
                            @if($order->closed == 1)
                            disabled
                            @endif>Підтвердити</button>
                    <form id="form-confirm-admin">
                        @csrf
                        <input type="hidden" name="order" value="{{$order->id}}">
                    </form>
                @endforeach

            </div>

        </div>
    </div>

@endsection