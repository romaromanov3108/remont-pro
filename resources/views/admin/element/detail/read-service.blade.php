@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <div class="container">
        <div class="row justify-content-md-center">
            @foreach($services as $service)
            <div class="col-md-8">
                <h4 style="text-align: center;">Редагувати послугу "{{$service->title}}"</h4>
                <form id="read-services" method="POST" action="/admin/add-service">
                    @csrf
                    <input type="hidden" name="service" value="{{$service->id}}">
                    <div class="form-group">
                        <label>Заголовок*</label>
                        <input type="text" class="form-control form-control-sm" name="title" placeholder="Чистка компютерів" value="{{$service->title}}">
                    </div>
                    <div class="form-group">
                        <label>Описання*</label>
                        <textarea class="form-control form-control-sm" name="description" placeholder="Опис полів" style="height: 300px;">{{$service->description}}</textarea>
                    </div>
                    <div class="form-group">
                        <img src="/img/service/{{$service->img}}" id="output">
                    </div>
                </form>
                <button class="btn btn-success btn-sm" id="btn-read-service"><ion-icon name="save"></ion-icon> Зберегти зміни</button>
                <div style="width: 100%; height: 50px;"></div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <div class="alert alert-warning">Ви не є адміністратором тому даний розділ для вас закритий!</div>
    @endif

@endsection