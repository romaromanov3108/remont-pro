@extends('admin.content')
@section('element')
    <div class="container">
        <div class="row justify-content-md-center">
            @foreach($orders as $order)
            <div class="col-md-4">

                    <h4 style="text-align: center;">Заказ №{{$order->id}}</h4>
                    <form id="my-read-form">
                        @csrf
                        <input type="hidden" name="order" value="{{$order->id}}">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control form-control-sm" value="{{$order->name or ""}}" placeholder="І'мя" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control form-control-sm" value="{{$order->phone or ""}}" placeholder="Номер телефону" required
                                @if($group > 0)
                                   disabled
                                @endif
                            >
                        </div>
                        <div class="form-group">
                            <input type="text" name="address" class="form-control form-control-sm" value="{{$order->adres or ""}}" placeholder="Адреса">
                        </div>
                        <div class="form-group">
                        <textarea name="problem"
                                  class="form-control form-control-sm"
                                  placeholder="Коментар (проблема, побажання клієнта)"
                                  style="height: 200px;">{{$order->problem or ""}}</textarea>
                        </div>
                        <div class="form-group col-lg-8">
                        <input type="text" name="sum" class="form-control form-control-sm" value="{{$order->suma or ""}}" placeholder="Сума">
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<select class="form-control form-control-sm" name="master">--}}
                        {{----}}
                        {{--</select>--}}
                        {{--</div>--}}
                        <div class="form-group">

                        <select class="form-control form-control-sm" name="status">
                        <option value="{{$order->status_id}}">{{\App\Status::where('id', $order->status_id)->value('title')}}</option>
                        @foreach(\App\Status::all() as $status)
                        @if($status->group >= $group and $status->type == "order")
                        <option value="{{$status->id}}" style="color: #fff; background: {{$status->background}};">{{$status->title}}</option>
                        @endif
                        @endforeach
                        </select>
                        </div>
                    </form>
                    @if($order->closed == 0)

                    <button class="btn btn-success btn-sm" id="btn-save-order"><ion-icon name="save"></ion-icon>Зберегти</button>
                        @if(count(\App\Confirm_order::where('order_id', $order->id)->get()) < 1)
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-read" id="btn-confirm-modal"><ion-icon name="checkmark"></ion-icon>Підтвердети</button>
                        @else
                            <button class="btn btn-primary btn-sm" disabled><ion-icon name="checkmark"></ion-icon> Підтвердити</button>
                        @endif
                    @endif
                    {{--<button class="btn btn-danger btn-sm" type="button" data-toggle="collapse" data-target="#collapse-receipt"><ion-icon name="md-print"></ion-icon> Квитанції</button>--}}

                        {{--<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-read">Підтвердити </button>--}}
                    @include('admin.modal.success_read')


            </div>
            {{--<div class="col-md-10 col-lg-offset-2">--}}
                {{--@include('admin.collapse.receipt')--}}
            {{--</div>--}}
            @endforeach

        </div>
    </div>

@endsection