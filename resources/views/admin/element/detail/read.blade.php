@extends('admin.content')
@section('element')

    <div class="container">
        <div class="row justify-content-md-center">

            <div class="col-md-4">
                @foreach($orders as $order)
                <h4 style="text-align: center;">Заказ №{{$order->id}}</h4>
                <form id="my-read-form">
                    @csrf
                    <input type="hidden" name="order" value="{{$order->id}}">
                    <div class="form-group">
                        <input type="hidden" name="name" class="form-control form-control-sm" value="{{$order->name or ""}}">
                        <input type="text" class="form-control form-control-sm" value="{{$order->name or ""}}" placeholder="І'мя" required disabled>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control form-control-sm" value="{{$order->phone or ""}}" placeholder="Номер телефону" required disabled>
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control form-control-sm" value="{{$order->adres or ""}}" placeholder="Адреса">
                    </div>
                    <div class="form-group">
                        <textarea name="problem"
                                  class="form-control form-control-sm"
                                  placeholder="Коментар (проблема, побажання клієнта)"
                                  style="height: 200px;">{{$order->problem or ""}}</textarea>
                    </div>
                    {{--<input type="text" name="sum" class="form-control form-control-sm" value="{{$order->suma or ""}}" placeholder="Сума">--}}
                    {{--<div class="form-group col-lg-8">--}}
                        {{--<input type="text" name="sum" class="form-control form-control-sm" value="" placeholder="Сума">--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<select class="form-control form-control-sm" name="master">--}}
                            {{----}}
                        {{--</select>--}}
                    {{--</div>--}}
                    @if(Auth::user()->group < 1)
                    <div class="form-group">

                        <select class="form-control form-control-sm" name="status">
                            <option value="{{$order->status_id}}">{{\App\Status::where('id', $order->status_id)->value('title')}}</option>
                            @foreach(\App\Status::all() as $status)
                                @if($status->group >= $group and $status->type == 'order')
                                   <option value="{{$status->id}}" style="color: #fff; background: {{$status->background}};">{{$status->title}}</option>
                              @endif
                            @endforeach
                        </select>
                    </div>
                    @endif
                </form>
                    <button class="btn btn-success btn-sm" id="btn-success-order"><ion-icon name="checkmark"></ion-icon> Взятись за заказ</button>
                    @if(Auth::user()->group < 1)
                        <button class="btn btn-primary btn-sm" id="btn-save-order"><ion-icon name="save"></ion-icon> Зберегти зміни</button>
                    @endif
                    {{--<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-read">Підтвердити </button>--}}
                    @endforeach
            </div>

        </div>
    </div>






@endsection