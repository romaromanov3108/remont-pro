@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <div class="container">
        <div class="row justify-content-md-center">

            <div class="col-md-8">
                <h4 style="text-align: center;">Створити послуги</h4>
                <form id="add-services" method="POST" action="/admin/add-service">
                    @csrf
                    <div class="form-group">
                        <label>Заголовок*</label>
                        <input type="text" class="form-control form-control-sm" name="title" placeholder="Чистка компютерів">
                    </div>
                    <div class="form-group">
                        <label>Описання*</label>
                        <textarea class="form-control form-control-sm" name="description" placeholder="Опис полів" style="height: 300px;"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Зображення</label>
                        <input type="file" name="img">
                    </div>
                </form>
                <button class="btn btn-success btn-sm" id="btn-add-service"><ion-icon name="add"></ion-icon> створити</button>

            </div>
        </div>
    </div>
    @else
        <div class="alert alert-warning">Ви не є адміністратором тому даний розділ для вас закритий!</div>
    @endif

@endsection