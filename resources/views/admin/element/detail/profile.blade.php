@extends('admin.content')
@section('element')
    <div class="container">
        <div class="row justify-content-md-center">

            <div class="col-md-6">
            <form id="profile-form">
                @csrf
                <table class="table char-mods table-sm">
                    <tbody>
                        <tr>
                            <td style="border-top: 0px;">Ім'я:</td>
                            <td style="border-top: 0px;"><input type="text" class="form-control form-control-sm" name="profil_name" value="{{Auth::user()->name}}"> </td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px;">Фамілія:</td>
                            <td style="border-top: 0px;"><input type="text" name="profil_surname" class="form-control form-control-sm" value="{{Auth::user()->surname}}"> </td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px;">Номер телефону:</td>
                            <td style="border-top: 0px;"><input type="text" name="profil_phone" class="form-control form-control-sm" value="{{Auth::user()->phone}}"> </td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px;">Навики:</td>
                            <td style="border-top: 0px;"><textarea name="profil_skills" class="form-control form-control-sm">{{Auth::user()->skills}}</textarea></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <button class="btn btn-success btn-sm" id="btn-profile-form">Зберегти</button>
            </div>

        </div>
    </div>
@endsection