@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
        <table class="table table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Ім'я</th>
                    <th>Номер телефону</th>
                    <th>емайл</th>
                    <th>Запитання</th>
                    <th>Відповідь</th>
                    <th>дата</th>
                    <th>Дії</th>


                </tr>
            </thead>
            <tbody>
                @foreach($questions as $question)
                <tr>
                    <td>{{$question->id}}</td>
                    <td>{{$question->name}}</td>
                    <td>{{$question->phone}}</td>
                    <td>{{$question->email}}</td>
                    <td>{{$question->question}}</td>
                    <td>
                        @if($question->answer == 1)
                            <ion-icon name="md-checkmark" style="color: green;"></ion-icon>
                            @else
                            <ion-icon name="md-remove" style="color: red;"></ion-icon>
                        @endif
                    </td>
                    <td>{{$question->created_at}}</td>
                    <td>
                        @if($question->answer == 0)
                            <a href="/admin/question/answer/{{$question->id}}" class="btn btn-sm btn-success">
                                <ion-icon name="md-checkmark" style="color: #fff;"></ion-icon>
                            </a>
                        @else
                            <a href="/admin/question/answer/{{$question->id}}" class="btn btn-sm btn-danger">
                                <ion-icon name="md-remove" style="color: #fff;"></ion-icon>
                            </a>
                        @endif
                    </td>

                </tr>
                @endforeach

            </tbody>
        </table>

    @endif
@endsection