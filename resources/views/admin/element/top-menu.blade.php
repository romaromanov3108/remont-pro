<nav class="navbar navbar-expand-lg navbar-dark bg-primary">


    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/admin">{{env('APP_NAME')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/order">Закази <span class="badge badge-warning">{{count(\App\Order::where('status_id', 1)->get())}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/order/my">Мої закази <span class="badge badge-success">{{count(\App\Order::where('master_id', Auth::id())->where('closed', 0)->get())}}</span></a>
            </li>
            @if(Auth::user()->group < 1)
            <li class="nav-item">
                <a class="nav-link" href="/admin/master">Майстри</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/confirm-order">Закази для підтвердженняи <span class="badge badge-success">{{count(\App\Confirm_order::where('confirm', 0)->get())}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/service">Послуги</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/question">Питання <span class="badge badge-question">{{count(\App\Question::where('answer', 0)->get())}}</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/blog">Блог</a>
            </li>
            @endif

        </ul>

    </div>
    <div class="btn-group" style="margin-right: 10px;">
        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{Auth::user()->name}}
        </button>
        <div class="dropdown-menu" style="margin-left: -20px;">
            <a class="dropdown-item" href="/admin/profile/">Мій профіль</a>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-rules">Правила</a>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-work">Порядок робіт</a>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-price">Прайс</a>
            <a class="dropdown-item" href="/file/receipt.pdf" target="_blank">Накладна PDF</a>
            {{--<div class="dropdown-divider"></div>--}}
            {{--<a class="dropdown-item" href="#">Separated link</a>--}}
        </div>
    </div>

    <a class="btn btn-danger btn-sm pull-right" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        вийти </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</nav>
