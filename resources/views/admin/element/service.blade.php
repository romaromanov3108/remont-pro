@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
    <a href="/admin/add-service" class="btn btn-primary btn-sm btn-create"><ion-icon name="add"></ion-icon> Створити послугу</a>

    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Фото</th>
                <th scope="col">Заголовок</th>
                <th scope="col">Описання</th>
                <th scope="col">дії</th>
            </tr>
        </thead>
        <tbody>
            @foreach($services as $service)
            <tr id="{{$service->id}}">
                <td>{{$service->id}}</td>
                <td>
                    <img src="/img/service/{{$service->img}}" width="100px">
                </td>
                <td style="width: 250px;">{{$service->title}}</td>
                <td>{{$service->description}}</td>
                <td style="width: 80px;">
                    <a href="/admin/read-service/{{$service->id}}" class="btn btn-primary btn-sm"><ion-icon name="create"></ion-icon></a>
                    <a href="/admin/deleted/service/{{$service->id}}" class="btn btn-danger btn-sm click-delet"><ion-icon name="trash"></ion-icon></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning">Ви не є адміністратором тому даний розділ для вас закритий!</div>
    @endif
@endsection