@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
        <a href="/admin/blog/create" class="btn btn-info btn-sm" style="float: right; margin:10px;">
            <ion-icon name="add"></ion-icon>
            Створити статтю
        </a>
        <a href="/admin/blog/categories" class="btn btn-info btn-sm" style="float: right; margin:10px;">
            <ion-icon name="ios-list"></ion-icon>
            Категорії
        </a>

        <table class="table table-sm">
            <thead>
            <tr>
                <th>ID</th>
                <th>Заголовок</th>
                <th>Опис</th>
                <th>url</th>
                <th>Перегляди</th>
                <th>дії</th>
            </tr>
            </thead>
            <tbody>
            @foreach(\App\Articles::all() as $article)
                <tr>
                    <td>{{$article->id}}</td>
                    <td>{{$article->title}}</td>
                    <td>{{$article->description}}</td>
                    <td>{{$article->latin_url}}</td>
                    <td>{{$article->view}}</td>
                    <td>
                        <a href="/admin/blog/read/{{$article->id}}" class="btn btn-info btn-sm"><ion-icon name="md-create"></ion-icon></a>
                        <a href="/admin/blog/delet/{{$article->id}}" class="btn btn-danger btn-sm"><ion-icon name="md-trash"></ion-icon></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


    @endif
@endsection