@extends('admin.content')
@section('verification')
    <div style="width: 100%; height: 30px;">
    </div>
    <h5 style="text-align: center;">Загрузіть будьласка фото своїх документів!</h5>
    <div class="container">
        <div class="row justify-content-md-center">

            <div class="col-md-4">
                <form id="form-verification">
                    @csrf
                    <div class="form-group">
                        <label>Перша сторінка паспорта</label>
                        <input type="file" name="pasport1">
                    </div>
                    <div class="form-group">
                        <label> Друга сторінка паспорта</label>
                        <input type="file" name="pasport2">
                    </div>
                    <div class="form-group">
                        <label>Сторінка з пропискою</label>
                        <input type="file" name="pasportProp">
                    </div>
                    <div class="form-group">
                        <label>Індифікаційний код</label>
                        <input type="file" name="indf_code">
                    </div>
                </form>
                <div class="btn btn-success btn-sm" id="btn_ver_form">Відправити</div>
            </div>
        </div>
    </div>

@endsection