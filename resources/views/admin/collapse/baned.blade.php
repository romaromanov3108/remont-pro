<div class="collapse" id="collapse-baned">
    <div class="card card-body">
        @if(count($baned) < 1)
            <div class="alert alert-success" role="alert">
                В майстра немає банів!
            </div>
        @else
       <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Причина</th>
                    <th scope="col">Дата бану</th>
                </tr>
            </thead>
           <tbody>
                @foreach($baned as $ban)
                    <tr>
                        <td>{{$ban->id}}</td>
                        <td>{{$ban->reason}}</td>
                        <td>{{$ban->created_at}}</td>
                    </tr>
                @endforeach
           </tbody>
       </table>
        @endif
    </div>
</div>