@extends('admin')
@section('content')
    @include('admin.element.top-menu')
    {{--@include('admin.element.left-menu')--}}
    <div class="container">
        <div class="row">
            <div class="col-lg">
                @if(Auth::user()->verification == 0)
                    <div class="alert alert-info" role="alert">
                        Будьласка подайте документи на <a href="/admin/verification">підтвердження</a> адміністратору, в випадку якщо в вже подали документи очікуйте підтвердження
                    </div>
                    @yield('verification')
                @elseif(Auth::user()->active == 1)
                    @yield('element')
                @else
                    <div class="alert alert-warning" role="alert">
                        Ваш обліковий запис не активовано, як тільки ваша кандидатура буде одобрена ви отримаєте доступ до розділів!
                    </div>
                @endif

            </div>
        </div>
    </div>
    @include('admin.element.footer')

@endsection