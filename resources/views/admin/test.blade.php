@extends('admin.content')
@section('element')
    @if(Auth::user()->group < 1)
        <form method="post" action="/admin/blog/create/test">
            @csrf
            <div class="form-group">
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control">
            </div>
            <input type="submit" class="btn btn-success" value="send">
        </form>


    @endif
@endsection