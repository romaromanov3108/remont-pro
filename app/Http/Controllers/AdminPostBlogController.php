<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Articles;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;
use Auth;
use DB;

class AdminPostBlogController extends Controller
{

    public function doCreateArticle(Request $request)
    {

        $id = $request->latin_url;
        $img = $request->file('prev_img');
        $ext = $img->getClientOriginalExtension();
        $nameTmp = $img->getBasename();
        $name = explode('.', $nameTmp);
        $name = $name[0].'.'.$ext;
        $imgSrc = public_path("img/blog/articles/".$id.'/');
        Storage::disk('upload')->makeDirectory('blog/articles/'.$id);

        Image::make($img)->resize(1280,600)->save($imgSrc.$name);

        $article = new Articles();

        $article->title = $request->title;
        $article->keywords = $request->keywords;
        $article->latin_url = $request->latin_url;
        $article->description = $request->description;
        $article->min_description = $request->min_description;
        $article->article = $request->article;
        $article->id_category = $request->id_category;
        $article->prev_img = $name;
        $article->autor_id = Auth()->id();
        $article->active = 1;
        $article->save();

        return 'article';
    }

    public function doReadArticle(Request $request)
    {
        $id = $request->id;
        Articles::where('id', $id)->update([
                    'title' => $request->title,
                    'latin_url' => $request->latin_url,
                    'keywords' => $request->keywords,
                    'description' => $request->description,
                    'min_description' => $request->	min_description,
                    'article' => $request->article,
                    'id_category' => $request->id_category,

        ]);

        return 'read';
    }

    public function doCreateCategories(Request $request, $type)
    {
        if($type == 'categories'){
            $category = new Categories();

            $category->name = $request->name;
            $category->latin_url = $request->latin_url;
            $category->save();

            return 'category';
        }
        elseif ($type == 'pod-cat'){

        }
    }

    public function doUploadImg(Request $request)
    {
        $path =  public_path("img/blog/articles/sumernote/");
        $file = $request->file('file');
        $filename = str_random(10) .'.' . $file->getClientOriginalExtension() ?: 'png';
        $img = Image::make($file);
        $img->save($path . $filename);
        echo '/img/blog/articles/sumernote/'.$filename;

    }

}
