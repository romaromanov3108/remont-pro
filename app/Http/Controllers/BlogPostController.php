<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mailing;
use App\Comments;
use Auth;
use Carbon\Carbon;

class BlogPostController extends Controller
{
    public function doCreatedComment(Request $request)
    {
        $date = Carbon::now('Europe/Kiev')->format('Y-m-d G:i:s');
        Comments::create([
            'id_article' => $request->article,
            'id_user' => Auth::user()->id,
            'comment' => $request->comment,
            'date' => $date,
        ]);

       return 'comment';
    }

    public function doMailing(Request $request)
    {
        $mailing = new Mailing();

        $mailing->email = $request->email;
        $mailing->ip = \Request::ip();
        $mailing->save();
        return 200;
    }
}
