<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use Auth;

class SuportController extends Controller
{
    public function doQuestion()
    {
       $questions = Question::all();
       $data = [
           'questions' => $questions
       ];

       return view('admin/element/question', $data);
    }
    public function doSuccesQuestion($id)
    {
        if(Auth::user()->group < 1) {
            if(Question::where('id', $id)->value('answer') == 0)
            {
                Question::where('id', $id)->update(['answer' => 1]);
            }else{
                Question::where('id', $id)->update(['answer' => 0]);
            }
            
            return redirect('/admin/question/');
        }else{
            return "error";
        }
    }
}
