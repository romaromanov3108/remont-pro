<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Question;
use App\Service;
use Mail;

class WelcomController extends Controller
{
    private function SendTelegram($values)
    {
        $token = "570438200:AAE2HC6dl-sVVazvjNzMHwQxXYtj__WwjeQ";
        $chat_id = "450257576";
        $str = '';
        foreach ($values as $key => $value)
        {
            $str .= "<b>".$key."</b>: ".$value."%0A";
        }
        $fp = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$str}", 'r');
        return true;
    }
    public function doWelcom()
    {
        $service = Service::all();
        $data = [
            'services' => $service
        ];
        return view('element.content', $data);
    }
   public function doOrder(Request $request)
   {
       $name = $request->name;
       $phone = $request->phone;

       $order = new Order();

       $order->name = $name;
       $order->phone = $phone;
       $order->status_id = 1;
       $order->forma_lend = $request->forma;
       $order->save();

       $data = [
           'type' => 'Новий заказ',
           'name' => $request->name,
           'phone' => $request->phone,
       ];
       Mail::send('mail/send-admin', $data, function($m){
           $m->to(env('ADMIN_MAIL'))->subject('Новий заказ');
       });
       $this->SendTelegram($data);
       return 'ok';
   }
   public function doQuestion(Request $request)
   {
        $question = new Question();

        $question->name = $request->name;
        $question->email = $request->email;
        $question->phone = $request->phone;
        $question->question = $request->question;
        $question->save();

        $data = [
            'type' => 'Питання',
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'question' => $request->question
        ];
        Mail::send('mail/send-admin', $data, function($m){
            $m->to(env('ADMIN_MAIL'))->subject('Запитання від користувачів');
        });
       $this->SendTelegram($data);

        return 'question';
   }
}
