<?php

namespace App\Http\Controllers;

use App\Master_verification;
use App\Service;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;
use App\Order;
use App\User;
use App\Baned;
use Auth;
use App\Confirm_order;

class AdminController extends Controller
{

    protected  function countOrder()
    {
       return count(Order::where('master_id', Auth::id())->where('closed', 0)->get());
    }

    public function doWelcom()
    {
//        $menu = $this->countOrder();

        return view('admin.element.welcom');
    }
    public function doDeletService($id)
    {
        if(Auth::user()->group < 1) {
            $title = Service::where('id', $id)->value('title');

            $img = Service::where('id', $id)->value('img');
            Storage::disk('upload')->delete('service/' . $img);
            Service::where('id', $id)->delete();

            $data = [
                'title' => $title,
                'id' => $id
            ];
            return $data;
        }else{
            return 'error';
        }
    }

    public function doDeletOrder($id)
    {
        if(Auth::user()->group < 1){
            Order::where('id', $id)->delete();
            return redirect('/admin/order');
        }else{
            return 'error';
        }

    }

    public function doService()
    {
//        $menu = $this->countOrder();
        $services = Service::all();
        $data = [
            'services' => $services
        ];
        return view('admin.element.service', $data);
    }

    public function doReadService($id)
    {
//        $menu = $this->countOrder();

        $service = Service::where('id', $id)->get();
        $data = [
            'services' => $service
        ];
        return view('admin.element.detail.read-service', $data);
    }

    public function doAddService()
    {
//        $menu = $this->countOrder();

        return view('admin.element.detail.add-service');
    }

    public function doOrder()
    {
//        $menu = $this->countOrder();
        $order = Order::where('master_id', null)->where('status_id', 1)->orderBy('created_at', 'desc')->get();
        if(User::where('id', Auth::id())->value('group') < 1)
        {
            $order = Order::orderBy('status_id', 'asc')->get();
        }
        $data = [
            'data' => $order,
        ];
        return view('admin.element.order', $data);
    }

    public function doProfile()
    {
//        $menu = $this->countOrder();

        return view('admin.element.detail.profile');
    }

    public function doActiveUser($id)
    {
        User::where('id', $id)->update([
            'status_id' => 8,
            'active' => 1
        ]);
        $master = User::where('id', $id)->value('name');
        $data = [
            'master' => $master,
            'type' => 'active'
        ];

        return $data;
    }

    public function doDeactiveUser($id)
    {
        User::where('id', $id)->update([
            'status_id' => 7,
            'active' => 0,
        ]);
        $master = User::where('id', $id)->value('name');
        $data = [
            'master' => $master,
            'type' => 'deactive'
        ];
        return $data;
    }

    public function doDetailMaster($id)
    {
        if(Auth::user()->group > 0){
            return 'error';
        }

        $menu = $this->countOrder();
        $masters = User::where('id', $id)->get();
        $orders = Order::where('master_id', $id)->get();
        $baned = Baned::where('user_id', $id)->get();
        $data = [
            'masters' => $masters,
            'baned' => $baned,
            'orders' => $orders
        ];
        return view('admin.element.detail.detail-master', $data);

    }

    public function doMyOrder()
    {
//        $menu = $this->countOrder();
        $order = Order::where('master_id', Auth::id())->orderBy('closed', 'asc')->get();
        $data = [
            'data' => $order,
        ];

        return view('admin.element.my-order', $data);
    }

    public function doMaster()
    {
//        $menu = $this->countOrder();
        $master = User::all();
        $data = [
            'masters' => $master
        ];
        return view('admin.element.master', $data);

    }

    public function doConfirm()
    {
//        $menu = $this->countOrder();
        $order = Confirm_order::join('orders', 'confirm_orders.order_id', '=', 'orders.id')->orderBy('closed', 'asc')->get();
        $data = [
            'orders' => $order,

        ];
        return view('admin.element.confirm', $data);
    }

    public function doConfirmRead($id)
    {
//        $menu = $this->countOrder();
        $order = Confirm_order::where('order_id', $id)->join('orders', 'confirm_orders.order_id', '=', 'orders.id')->get();
        $data = [
            'orders' => $order,
        ];
        return view('admin.element.detail.my-confirm', $data);
    }

    public function doVerification()
    {
//        $menu = $this->countOrder();

        return view('admin.element.verification');
    }

    public function doRead($id)
    {
//        $menu = $this->countOrder();
        $orders = Order::where('id', $id)->get();
        $group = User::where('id', Auth::id())->value('group');
        $data = [
            "orders" => $orders,
            'group' => $group,

        ];
        return view('admin.element.detail.read', $data);
    }

    public function doMyRead($id)
    {
//        $menu = $this->countOrder();
        $order = Order::where('id', $id)->where('master_id', Auth::id())->get();
        $group = User::where('id', Auth::id())->value('group');

        $data = [
            'orders' => $order,
            'group' => $group,
        ];
        return view('admin.element.detail.my-order', $data);
    }

    public function doSuccessVerifi()
    {
        $verifi = Master_verification::join('users', 'master_verification.master_id', '=', 'users.id')->get();

        $data = [
            'master' => $verifi
        ];
        return view('admin.element.detail.success-verifi', $data);
    }

    public function doSuccessMasterVerifi($id)
    {
        if(Auth::user()->group < 1){
            User::where('id', $id)->update([
                'status_id' => 10,
                'verification' => 1
            ]);
            Master_verification::where('master_id', $id)->update([
                'verification' => 1
            ]);
            return 'ok';
        }else{
           return redirect('/admin/');
        }
        return $id;
    }

}
