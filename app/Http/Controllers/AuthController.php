<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\User;
use Redirect;
use Session;

class AuthController extends Controller
{
    // Some methods which were generated with the app

    /**
     **_ Redirect the user to the OAuth Provider.
    _**
     **_ @return Response
    _**/
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

//    protected $redirectTo = '/blog';
    public function handleProviderCallback($provider)
    {
    $user = Socialite::driver($provider)->user();

    $authUser = $this->findOrCreateUser($user, $provider);

    Auth::login($authUser, true);

        return redirect('/blog/atricle/'.session('articleLink'));
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
//        $id = User::where('email', $user->email)->value('id');
//        if($id !== null){
//            return $authUser;
//        }else {
            return User::create([
                'name' => $user->name,
                'email' => $user->email,
                'provider' => $provider,
                'provider_id' => $user->id,
                'avatar' => $user->avatar,
            ]);
//        }
    }
}
