<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use Storage;
use File;
use Carbon\Carbon;

class SitemapController extends Controller
{
    public function doPut()
    {
        $heder = '<?xml version="1.0" encoding="UTF-8"?>
                   <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                   ';


        $end = '</urlset>';

//        File::put('sitemap.xml', $heder);
        $date = Carbon::now('Europe/Kiev')->format('Y-m-d');
        $time = Carbon::now('Europe/Kiev')->format('G:h:s');
        $dateTime = $date.'T'.$time.'+02:00';
        File::put('sitemap.xml', $heder);

        foreach (Articles::all() as $article){
            $url = \URL::to('/').'/blog/atricle/'.$article->latin_url;
            $sitemap = '<url>
                                    <loc>'.$url.'</loc>
                                    <lastmod>'.$dateTime.'</lastmod>
                                </url>
                                ';
            File::append('sitemap.xml', $sitemap);
        }

        File::append('sitemap.xml', $end);

        $data = File::get('sitemap.xml');
        dd($data);

    }

    public function doAppend()
    {

    }
}
