<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function doTest()
    {
        return view('admin.test');
    }
    public function doPostTest(Request $request)
    {
        dd($request->all());
    }

}
