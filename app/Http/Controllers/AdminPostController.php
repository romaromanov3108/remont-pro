<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Intervention\Image\ImageManagerStatic as Image;
use Storage;
use App\Order;
use App\Service;
use App\User;
use Auth;
use App\Master_verification;
use App\Baned;
use App\Confirm_order;

class AdminPostController extends Controller
{
    public function doSuccessOrder(Request $request)
    {
        $status = 0;
//        return $request->path();
//        перевырка кількості активних заказів майстра

        $order = Order::find($request->order);
        $orderAll = Order::where('master_id', Auth::id())->where('closed', 0)->get();
        if (count($orderAll) > 2) {
            $status = 1;

            $data = [
                'type' => 'success',
                'status' => $status
            ];
            return $data;
        }
//      Перевірка чи заказ вільни
        if ($order->master_id !== null) {
            $status = 2;

            $data = [
                'type' => 'success',
                'status' => $status
            ];
            return $data;
        }


        $order->master_id = Auth::id();
        $order->adres = $request->address;
        $order->problem = $request->problem;
        $order->status_id = 3;

        $order->save();

        $data = [
            'type' => 'success',
            'status' => $status
        ];
//
        return $data;
    }

    public function doProfileForm(Request $request)
    {

        Auth::user()->update([
            'name' => $request->profil_name,
            'surname' => $request->profil_surname,
            'phone' => $request->profil_phone,
            'skills' => $request->profil_skills
        ]);
//

        $data = [
            'type' => 'profile',
            'status' => 0
        ];
        return $data;
    }

    public function doConfirmForm(Request $request)
    {
        $img = $request->file('photo');
        $ext = $img->getClientOriginalExtension();
        $nameTmp = $img->getBasename();
        $name = explode('.', $nameTmp);
        $name = $name[0].'.'.$ext;
        $imgSrc = public_path("img/confirm-order/user/".Auth::id().'/');
        Storage::disk('upload')->makeDirectory('confirm-order/user/'.Auth::id());

        Image::make($img)->save($imgSrc.$name);
        $status = Order::where('id', $request->order)->value('status_id');
        $confirm = new Confirm_order();

        $confirm->order_id = $request->order;
        $confirm->status_id_confirm = $status;
        $confirm->comment = $request->comment;
        $confirm->photo = Auth::id().'/'.$name;
        $confirm->save();

        return "ok";
    }
    public function doAddService(Request $request)
    {
        $img = $request->file('img');
        $ext = $img->getClientOriginalExtension();
        $nameTmp = $img->getBasename();
        $name = explode('.', $nameTmp);
        $name = $name[0].'.'.$ext;
        $imgSrc = public_path("img/service/");

        Image::make($img)->widen(81)->save($imgSrc.$name);

        $service = new Service();

        $service->title = $request->title;
        $service->description = $request->description;
        $service->img = $name;
        $service->save();

        $data = [
            'type' => 'add-service',
        ];
        return $data;
    }

    public function doReadService(Request $request)
    {

        Service::where('id', $request->service)->update([
            'title' => $request->title,
            'description' => $request->description
        ]);
        $data = [
            'type' => 'read-service',
            'data' => $request->all()
        ];
        return $data;
    }

    public function doMasterBaned(Request $request)
    {
        $baned = new Baned();

        $baned->user_id = $request->id;
        $baned->reason = $request->reson;
        $baned->ban = 1;
        $baned->save();
        User::where('id', $request->id)->update([
            'active' => 0,
            'status_id' => 9
        ]);

        $data = [
            'type' => 'baned',
        ];
        return $data;
    }

    public function doSaveRead(Request $request)
    {
//        return $request->all();
        $status = 0;
        if(Order::where('id', $request->order)->value('master_id') == Auth::id() or Auth::user()->group < 1){
            Order::where('id', $request->order)->update([
                'name' => $request->name,
                'adres' => $request->address,
                'problem' => $request->problem,
                'suma' => $request->sum,
                'status_id' => $request->status,
            ]);
        }else{
            $status = 1;
        }


        $data = [
            'type' => 'save',
            'status' => $status
        ];


        return $data;
    }
    public function doVerifiForm(Request $request)
    {
        $arr = [];

        foreach($request->allFiles() as $data)
        {
            $ext = $data->getClientOriginalExtension();
            $nameTmp = $data->getBasename();
            $name = explode('.', $nameTmp);
            $name = $name[0].'.'.$ext;
            $imgSrc = public_path("img/verification/user/".Auth::id().'/');
            Storage::disk('upload')->makeDirectory('verification/user/'.Auth::id());
            Image::make($data)->save($imgSrc.$name);

            array_push($arr, $name);
        }
        $nameImg = implode(',', $arr);
        $img = new Master_verification();

        $img->master_id = Auth::id();
        $img->document_photo = $nameImg;
        $img->verification = 0;
        $img->save();

        return $nameImg;
    }
    public function doConfirmOrder(Request $request)
    {
        Order::where('id', $request->order)->update([
            'closed' => 1,
            'status_id' => 6
        ]);
        Confirm_order::where('order_id', $request->order)->update(['confirm' => 1]);
        $data = [
            'type' => 'confirm',
            'data' => $request->all()
        ];
        return $data;
    }

}
