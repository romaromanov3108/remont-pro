<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use phpDocumentor\Reflection\Location;
use App\Mailing;
use App\Categories;
use App\User;
use Session;
use App\Like;
use Auth;
use App\Comments;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function doWelcom()
    {

        $articles = Articles::join('users', 'autor_id' ,'=', 'users.id')
//                        ->join('categories', 'id_category', '=', 'categories.id')
                        ->select('articles.*','users.name')
                        ->get();

        $data = [
            'articles' => $articles,
        ];
        return view('blog/element/welcom', $data);
    }
    public function doArticle($url)
    {

        session(['articleLink' => $url]);
        Articles::where('latin_url', $url)->increment('view');

        $article = Articles::where('latin_url', $url)
            ->join('users', 'autor_id', '=', 'users.id')
            ->select('articles.*', 'users.name')
            ->get();
        $description = $article[0]->description;
        $keywords = $article[0]->keywords;
        $imgPath = 'http://catharsis-service.com.ua/img/blog/articles/'.$article[0]->latin_url.'/'.$article[0]->prev_img;

        $comments = Comments::where('id_article', $article[0]->id)
            ->join('users', 'id_user', '=', 'users.id')
            ->orderBy('date', 'desc')
            ->get();

        $data = [
            'keywords' => $keywords,
            'description' => $description,
            'articles' => $article,
            'title' => $article[0]->title,
            'minDesc' => $article[0]->min_description,
            'imgPath' => $imgPath,
            'comments' => $comments

        ];
//        dd($comments);
        return view('blog.element.detail.article', $data);
    }

    public function doCategories($latin)
    {
        $id = Categories::where('latin_url', '/blog/'.$latin)->value('id');
        $articles = Articles::where('id_category', $id)->join('users', 'autor_id' ,'=', 'users.id')
                            ->select('articles.*','users.name')
                            ->get();
            $data = [
                'articles' => $articles,
            ];

        return view('blog/element/welcom', $data);


    }

    public function doLike($id)
    {
        $like = Like::where('id_article', $id)->where('id_user', Auth::user()->id)->first();
        if($like === null){
            Like::create([
                'id_article' => $id,
                'id_user' => Auth::user()->id,
            ]);
            Articles::where('id', $id)->increment('like');
        }else{
            return 'stop';
        }
        return 'ok';

    }


}
