<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Articles;

class AdminBlogController extends Controller
{
    public function doWelcom ()
    {
        return view('admin.element.blog');
    }

    public function doCategories()
    {
        $categories = Categories::all();

        $data = [
            'categories' => $categories,
        ];
        return view('admin.element.categories', $data);
    }

    public function doCreateArticle()
    {
        return view('admin.element.detail.article-create');

    }

    public function doDeletArticle($id)
    {
        Articles::where('id', $id)->delete();

        return redirect('/admin/blog');
    }

    public function doReadArticle($id)
    {
        $articles = Articles::where('id', $id)->get();

        $data =[
            'articles' => $articles
        ];
        return view('admin/element/detail/article-read', $data);
    }

}
