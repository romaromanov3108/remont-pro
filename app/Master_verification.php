<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_verification extends Model
{
    protected $table="master_verification";

    public function user()
    {
        return $this->belongsTo('app\User', 'master_id', 'id');
    }
}
