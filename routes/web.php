<?php
use Spatie\Sitemap;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomController@doWelcom');
Route::post('/send-form', 'WelcomController@doOrder');
Route::post('/question-form', 'WelcomController@doQuestion');

Auth::routes();

Route::get('/auth/{provider}', 'AuthController@redirectToProvider');
Route::get('/auth/{provider}/callback', 'AuthController@handleProviderCallback');

Route::group(['middleware' => 'auth'], function(){
    Route::group(['prefix' => 'admin'], function(){
       Route::get('/', 'AdminController@doWelcom');
       Route::get('/order', 'AdminController@doOrder');
       Route::get('/service', 'AdminController@doService');
       Route::get('/add-service', 'AdminController@doAddService');
       Route::get('/read-service/{id}', 'AdminController@doReadService');
       Route::get('/profile', 'AdminController@doProfile');
       Route::get('/order/my', 'AdminController@doMyOrder');
       Route::get('/master', 'AdminController@doMaster');
       Route::get('/read/{id}', 'AdminController@doRead');
       Route::get('/read/my/{id}', 'AdminController@doMyRead');
       Route::get('/confirm-order', 'AdminController@doConfirm');
       Route::get('/verification', 'AdminController@doVerification');
       Route::get('/confirm/order/{id}', 'AdminController@doConfirmRead');
       Route::get('/master/active/{id}', 'AdminController@doActiveUser');
       Route::get('/master/deactive/{id}', 'AdminController@doDeactiveUser');
       Route::get('/deleted/service/{id}', 'AdminController@doDeletService');
       Route::get('/deleted/order/{id}', 'AdminController@doDeletOrder');
       Route::get('/master/detail/{id}', 'AdminController@doDetailMaster');
       Route::get('/success-verification', 'AdminController@doSuccessVerifi');
       Route::get('/verifi-master/{id}', 'AdminController@doSuccessMasterVerifi');


       Route::post('/success-order', 'AdminPostController@doSuccessOrder');
       Route::post('/profile-form', 'AdminPostController@doProfileForm');
       Route::post('/add-service', 'AdminPostController@doAddService');
       Route::post('/read-service', 'AdminPostController@doReadService');
       Route::post('/master-baned', 'AdminPostController@doMasterBaned');
       Route::post('/confirm-form-admin', 'AdminPostController@doConfirmOrder');
       Route::post('/confirm-form', 'AdminPostController@doConfirmForm');
       Route::post('/save-order', 'AdminPostController@doSaveRead');
       Route::post('/verification-form', 'AdminPostController@doVerifiForm');
       Route::post('/feedback-form', 'AdminPostController@doFeedback');

       Route::get('/question', 'SuportController@doQuestion');
       Route::get('/question/answer/{id}', 'SuportController@doSuccesQuestion');
        Route::group(['prefix' => 'blog'], function(){
            Route::get('/', 'AdminBlogController@doWelcom');
            Route::get('/delet/{id}', 'AdminBlogController@doDeletArticle');
            Route::get('/categories', 'AdminBlogController@doCategories');
            Route::get('/create', 'AdminBlogController@doCreateArticle');
            Route::get('/read/{id}', 'AdminBlogController@doReadArticle');
            Route::post('/create/articles', "AdminPostBlogController@doCreateArticle");
            Route::post('/read/article', "AdminPostBlogController@doReadArticle");
            Route::post('/create/{category}', 'AdminPostBlogController@doCreateCategories');
            Route::post('sumernote/upload', 'AdminPostBlogController@doUploadImg');
//
        });
    });
});
Route::group(['prefix' => 'blog'], function() {
    Route::get('/', "BlogController@doWelcom");
    Route::get('/atricle/{latinurl}', "BlogController@doArticle");
    Route::get('/{latin_url}', "BlogController@doCategories");
    Route::get('/like/{id}', 'BlogController@doLike');

    Route::get('/article/like/{id}', 'BlogController@doLike'); //no active

    Route::post('/mailing', 'BlogPostController@doMailing');
    Route::post('/create/comment', 'BlogPostController@doCreatedComment');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/sitemap', 'SitemapController@doPut');