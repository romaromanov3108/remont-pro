$(document).ready(function(){

  var link = window.location.pathname;

  var lis = $('a[href="'+link+'"]').parent();
  lis.attr('class', 'nav-item active');


    function alertGo(url, status, type)
    {
        if(status == 0) {
           switch(type){
               case 'success':
                   $('.ok').html('Замовлення закріплено за вами!');
                   break;
               case 'save':
                   $('.ok').html('Зміни збережено');
                   break;
               case 'add-service':
                   $('.ok').html('Послугу створено!');
                   break;
               case 'read-service':
                   $('.ok').html('Зміни збережено');
               case 'baned':
                   $('.ok').html('Користувач заблокований');
           }

            $('.ok').show();
            setTimeout(function () {
                $(".ok").fadeOut("slow")
            }, 4000);
        }else if(status == 1){
            $('.stop').html('Максимально допустима кількість відкритих заказів 3');
            $('.stop').show();
            setTimeout(function () {
                $(".stop").fadeOut("slow")
            }, 4000);
        }else if(status == 2){
            $('.stop').html('Для даного замовлення вже визначено майстра');
            $('.stop').show();
            setTimeout(function () {
                $(".stop").fadeOut("slow")
            }, 4000);
        }
        if(type == 'success' || type == 'add-service' || type == 'read-service') {
            setTimeout(function () {
                $(location).attr('href', url);
            }, 4500);
        }
    }

    function ajax(data, url)
    {
        $.ajax({
            url: url,
            type: 'post',
            processData: false,
            contentType: false,
            // dataType: 'json',
            data: data,
            success:function(data) {
                console.log(data);
                if(data.type == 'success'){
                    alertGo('/admin/order/my', data.status, data.type);
                }else if(data.type == 'save'){

                    alertGo(location.pathname, data.status, data.type);
                }else if(data.type == 'confirm'){

                    $('#btn-confirm-admin').attr('disabled',true);

                    $('.ok').html('Замовлення підтвердженно!');
                    $('.ok').show();
                    setTimeout(function () {
                        $(".ok").fadeOut("slow")
                    }, 4000);
                }else if(data.type == 'profile'){
                    alertGo('/admin/profile', '0', 'save');
                }else if(data.type == 'add-service'){

                    alertGo('/admin/service', 0, data.type);
                }else if(data.type == 'read-service') {

                    alertGo('/admin/service', 0, data.type);
                }else if(data.type == 'baned'){

                    alertGo('no', 0, 'baned');
                }else
                {
                    // console.log(data);
                    $('#modal-read').modal('hide');
                    $('#btn-confirm-modal').attr('disabled',true);

                    $('.ok').html('Замовлення відправлено для підтвердження!');
                    $('.ok').show();
                    setTimeout(function () {
                        $(".ok").fadeOut("slow")
                    }, 4000);
                }
                // $(location).attr('href', '/admin/order/');
                // alertGo('/admin/order/', data);

            },
            error:function() {

            }
        });
    }
    function stop(selectors){
        $('.stop').html('Заповніть необхідні поля!');
        selectors.css('border', '1px solid red');
        $('.stop').show();
        setTimeout(function () {
            $(".stop").fadeOut("slow")
        }, 4000);
    }

  $('#btn-success-order').click(function(e){
      e.preventDefault();
      // console.log('okokokoksok');
      var forma = $('#my-read-form');
      var data = new FormData(forma.get(0));
      var url = '/admin/success-order';

      ajax(data, url);
  });
  $('#btn-save-order').click(function(e){
      e.preventDefault();

      var forma = $('#my-read-form');
      var data = new FormData(forma.get(0));
      var url = '/admin/save-order';
      ajax(data, url);
  });


//confirm

  $('#btn-form-comfirm').click(function(e){
      e.preventDefault();
      var forma = $('#form-confirm');
      var data = new FormData(forma.get(0));
      var url = "/admin/confirm-form";
      if(data.get('comment') == ""){

          $('.stop').html('Поле "коментарі" повинно бути заповнене!');
          $('[name=comment]').css('border', '1px solid red');
          $('.stop').show();
          setTimeout(function () {
              $(".stop").fadeOut("slow")
          }, 4000);
          // console.log(data.get('photo').size);
          return console.log('stop');
      }
      if(data.get('photo').size < 1){
          $('.stop').html('Потрібно завантажити фотографію накладної!');
          $('[name=photo]').css('border', '1px solid red');
          $('.stop').show();
          setTimeout(function () {
              $(".stop").fadeOut("slow")
          }, 4000);

          return console.log('stop');
      }

      console.log('start');
      ajax(data, url);

    });
  $('#btn-confirm-admin').click(function(e){
      e.preventDefault();
      var forma = $('#form-confirm-admin');
      var data = new FormData(forma.get(0));
      var url = "/admin/confirm-form-admin";
      ajax(data, url);

  });
  $('#btn-profile-form').click(function(e){
      e.preventDefault();
      var forma = $('#profile-form');
      var data = new FormData(forma.get(0));
      var url = "/admin/profile-form";
      if(data.get('profil_name') == ''){
        var sel = $('[name=profil_name]');
        stop(sel);
        return console.log('stop');
      }
      if(data.get('profil_surname') == ''){
        var sel = $('[name=profil_surname]');
        stop(sel);
        return console.log('stop');
      }
      if(data.get('profil_phone') == ''){
          var sel = $('[name=profil_phone]');
          stop(sel);
          return console.log('stop');
      }
      ajax(data, url);
  });
  $('.btn-click').click(function(e){
      e.preventDefault();
      var url = $(this).attr('href');
      $.get(url).done(function(data){
          $('a[href="'+url+'"]').remove();
          if(data.type == 'active'){
              $('.ok').html('Майстер '+data.master+' активований!');
          }else if(data.type == 'deactive'){
              $('.ok').html('Майстер '+data.master+' деактивований!');
          }
          $('.ok').show();
          setTimeout(function () {
              $(".ok").fadeOut("slow")
          }, 4000);
      });

  });

  $('.click-delet').click(function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.get(url).done(function(data){


            $('.ok').html('Послугу "'+data.title+'" видалено');
            $('#'+data.id).remove();
            $('.ok').show();
            setTimeout(function () {
                $(".ok").fadeOut("slow")
            }, 4000);

        });
  });



    $('#btn-add-service').click(function (e) {
        e.preventDefault();
        var url = "/admin/add-service";
        var forma = $('#add-services');
        var data = new FormData(forma.get(0));

        if(data.get('title') == ''){
            var sel = $('[name=title]');
            return stop(sel);
        }

        if(data.get('description').length < 10){
            var sel = $('[name=description]');
            return stop(sel);
        }
        if(data.get('img').size < 2){
            var sel = $('[name=img]');
            return stop(sel);
        }

        ajax(data, url);
  });


  $('#btn-read-service').click(function (e) {
        e.preventDefault();
        var url = '/admin/read-service';
        var forma = $('#read-services');
        var data = new FormData(forma.get(0));
        if(data.get('title') == ''){
          var sel = $('[name=title]');
          return stop(sel);
        }

        if(data.get('description').length < 10){
          var sel = $('[name=description]');
          return stop(sel);
        }
        ajax(data, url);
  });

  $('#btn-baned-form').click(function(e){
      e.preventDefault();
      var url = '/admin/master-baned';
      var forma = $('#baned-form');
      var data = new FormData(forma.get(0));
      if(data.get('reson' == '')){
          var sel = $('[name=reson]');
          return stop(sel);
      }
      ajax(data, url);
  });
  $('#btn_ver_form').click(function(e){
      e.preventDefault();
      var url = '/admin/verification-form';
      var forma = $('#form-verification');
      var data = new FormData(forma.get(0));
      if(data.get('pasport1').size < 10){
          var sel = $('[name=pasport1]');
          return stop(sel);
      }
      if(data.get('pasport2').size < 10){
          var sel = $('pasport2');
          return stop(sel);
      }
      if(data.get('pasportProp').size < 10){
          var sel = $('pasportProp');
          return stop(sel);
      }
      if(data.get('indf_code').size < 10){
          var sel = $('Indf_code');
          return stop(sel);
      }
      ajax(data, url);

  });

});

