$(document).ready(function() {

    function ajax(url, data)
    {
        $.ajax({
            url: url,
            type: 'post',
            processData: false,
            contentType: false,
            // dataType: 'json',
            data: data,
            success:function(data) {
                console.log(data);
                if(data == 'category') {
                    $('#modal-category').modal('hide');
                    alert('Категорію створено!');
                    location.reload();

                }
                if(data == 'article'){
                    $("#article-form")[0].reset();
                    alert('Статтю створено!');
                    setTimeout(function () {
                        location.replace('/admin/blog');
                        }, 3500);
                }
                if(data == 'read'){
                    alert('Зміни збережено!');
                }
            },
            error:function() {

            }
        });
    }
    function alert(text)
    {
        $('.ok').html(text);
        $('.ok').show();
        setTimeout(function () {
            $(".ok").fadeOut("slow")
        }, 3000);
    }
    function stop()
    {
        $('.stop').html('Заповніть необхідні поля!');
        $('.stop').show();
        setTimeout(function () {
            $(".stop").fadeOut("slow")
        }, 4000);
    }

    function validators(data, arr) {
        var end = arr.length,
            eror = 0;

        for(var i=0; i < end; i++){

            if(data.get(arr[i]) == ''){
                eror++;
                $('[name='+arr[i]+']').css('border', '1px solid red');
            }
        }
        return eror;
    }

    $('#btn_article_cr').click(function(e){

        e.preventDefault();
        var forma = $('#article-form');
        var data = new FormData(forma.get(0));
        var url = '/admin/blog/create/articles';
        var arr = ['title', 'description', 'keywords', 'latin_url', 'min_description', 'article'];

        if(validators(data, arr) > 0){
            stop();
            return false
        }else{
            ajax(url,data);
        }

    });

    $('#btn_article_read').click(function(e){

        e.preventDefault();
        var forma = $('#article-read-form');
        var data = new FormData(forma.get(0));
        var url = '/admin/blog/read/article';
        var arr = ['title', 'description', 'keywords', 'latin_url', 'min_description', 'article'];

        if(validators(data, arr) > 0){
            stop();
            return false
        }else{
            ajax(url,data);
        }

    });

    $('#btn_create_categories').click(function(e){
        e.preventDefault();
        var forma = $('#create_categories');
        var data = new FormData(forma.get(0));
        var url = '/admin/blog/create/categories';
        var eror = 0;

        if(data.get('name') == '')
        {
            eror++;
            $('[name=name]').css('border', '1px solid red');
        }
        if(data.get('latin_url') == ''){
            eror++;
            $('[name=latin_url]').css('border', '1px solid red');
        }
        if(eror > 0){
            stop();
            return false;
        }else{
            ajax(url, data);
        }

    });
});
