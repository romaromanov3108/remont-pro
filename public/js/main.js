
function ajax(data, url)
{
    if(url == null){
        url = '/send-form';
    }

    $.ajax({
        url: url,
        type: 'post',
        processData: false,
        contentType: false,
        // dataType: 'json',
        data: data,
        success:function(data) {
            // console.log(data);
           // if(data == 'question'){
           //     $('.ok').html('Дякуємо вам за звернення, ми вам обов\'язково відповімо');
           // }else {
           //
           //     $('.ok').html('Дякуємо вам за звернення, найближчим часом наш спеціаліст з вами звяжеться');
           // }
           //  $('.ok').show();
           //
           //  setTimeout(function () {
           //      $(".ok").fadeOut("slow")
           //  }, 4000);

        },
        error:function() {

        }
    });
}
function stop(){
    $('.stop').html('Заповніть необхідні поля!');
    $('.stop').show();
    setTimeout(function () {
        $(".stop").fadeOut("slow")
    }, 4000);
}
function ok(data){
    if(data == 'question'){
        $('.ok').html('Дякуємо вам за звернення, ми вам обов\'язково відповімо');
    }else {

        $('.ok').html('Дякуємо вам за звернення, найближчим часом наш спеціаліст з вами звяжеться');
    }
    $('.ok').show();

    setTimeout(function () {
        $(".ok").fadeOut("slow")
    }, 4000);
}

function validator(data) {
    var eror = 0;
    if(data.get('name') == ''){
        $('#'+data.get('forma')+' [name=name]').css('border', '1px solid red');
        stop();
        eror++;
    }
    if(data.get('phone') == ''){
        $('#'+data.get('forma')+' [name=phone]').css('border', '1px solid red');
        stop();
        eror++;
    }
    if(eror == 0){
        ajax(data);
        if(data.get('forma') == 'form-call') {
            $('#modal-form').modal('hide');
        }
    }

}

$('#btn_header_form').click(function(e){
    e.preventDefault();
    var forma = $('#header_form');
    var data =  new FormData(forma.get(0));

    validator(data);
    forma[0].reset();
    ok();

});
$('#btn_alert').click(function(e){
    e.preventDefault();
    var forma = $('#form-call');
    var data =  new FormData(forma.get(0));

    validator(data);
    forma[0].reset();
    ok();

});
$('#btn_service').click(function(e){
    e.preventDefault();
    var forma = $('#form-service');
    var data =  new FormData(forma.get(0));

    validator(data);
    forma[0].reset();
    ok();
});
$('#btn_about').click(function(e){
    e.preventDefault();
    var forma = $('#form-about');
    var data = new FormData(forma.get(0));

    validator(data);
    forma[0].reset();
    ok();
});
$('#btn_feedback').click(function (e) {
    e.preventDefault();
    var forma = $('#form-feedback');
    var data = new FormData(forma.get(0));
    var error = 0;
    $('#form-feedback + [name=name]').css('border', '1px solid red');

    if(data.get('name') == ''){
        $('#form-feedback [name=name]').css('border', '1px solid red');
        error++;
    }

    if(data.get('email') == ''){
        $('#form-feedback [name=email]').css('border', '1px solid red');
        error++;
    }

    if(data.get('qestion') == ''){
        $('#form-feedback [name=qestion]').css('border', '1px solid red');
        error++;
    }

    if(error > 0){
        stop();
        return 'stop';
    }else{
        var url = '/question-form';

        ajax(data, url);
        forma[0].reset();
        $('.feedback').hide('slow');
        $('#popup__toggle').show('slow');
        ok('question');
    }
});
$('#popup__toggle').click(function (e) {
    $('.feedback').show('slow');
    $('#popup__toggle').hide();
});
$('#closed_feedback').click (function(){
   $('.feedback').hide('slow');
    $('#popup__toggle').show('slow');
});