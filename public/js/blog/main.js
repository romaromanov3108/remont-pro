$(document).ready(function(){
    function ajax(url, data)
    {
        $.ajax({
            url: url,
            type: 'post',
            processData: false,
            contentType: false,
            // dataType: 'json',
            data: data,
            success:function(data) {
                console.log(data);
                if(data == 200){
                    $('#mailing_form')[0].reset();
                    $('.ok').html('Підписку оформлено, дякуємо!');
                    $('.ok').show();
                    setTimeout(function () {
                        $(".ok").fadeOut("slow")
                    }, 4000);
                }
                if(data == 'comment'){
                    location.reload();
                    $('.ok').html('Коментра Додано!');
                    $('.ok').show();
                    setTimeout(function () {
                        $(".ok").fadeOut("slow")
                    }, 4000);
                }
                }
            },
            error:function(){

            }
        });
    }

    function stop(text){
        $('.stop').html(text);
        $('.stop').show();
        setTimeout(function () {
            $(".stop").fadeOut("slow")
        }, 4000);
    }

    $('#btn_mailing').click(function(e){
        e.preventDefault();
        var forma = $('#mailing_form');
        var data = new FormData(forma.get(0));
        var url = '/blog/mailing';

        if(data.get('email') == ''){
            $('[name=email]').css('border', '1px solid red');
            stop('Заповніть будьласка email!');
            return false;
        }else{
            ajax(url, data);
        }
    });

    $('.like').click(function(){
        var idArticle = $(this).attr('data-id');
        var url = '/blog/like/'+idArticle;
        console.log(url);
        $.get(url, function(data){
            console.log(data);
            if(data === 'ok'){
                $('.like').css('color', 'red');
                var count = $('.count').html();
                count = Number(count)+1;
                $('.count').html(count);
            }else{
                stop('Ви вже поставили лайк!');
            }
        });
    });
    $('#btn-comment').click(function (e){
        e.preventDefault();
        var form = $('#comment-form');
        var data = new FormData(form.get(0));
        var url = '/blog/create/comment';
        if(data.get('comment') == ''){
            $('[name=comment]').css('border', '1px solid red');
            stop('Заповніть обв\'язкові поля!');
        }else {
            ajax(url, data)
        }
    });


});